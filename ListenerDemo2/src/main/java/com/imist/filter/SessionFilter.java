package com.imist.filter;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String loginUser = (String) httpRequest.getSession().getAttribute("LoginUser");
        if (loginUser == null) {
            httpResponse.sendRedirect(httpRequest.getContextPath() + "/index.jsp?flag=1");
        } else {
            chain.doFilter(request, response);
        }
    }

    public void destroy() {

    }
}
