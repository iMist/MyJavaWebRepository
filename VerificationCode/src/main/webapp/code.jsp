<%@ page import="com.imist.code.CaptcahCode" %><%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/14
  Time: 下午8:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    //因为浏览器默认会对图片等资源进行缓存操作，清空浏览器缓存，防止因为浏览器缓存导致刷新无效
    response.setHeader("pragma","no-cache");
    response.setHeader("cache-control","no-cache");
    response.setHeader("expires", "0");

    String code = CaptcahCode.drawImage(response);
    session.setAttribute("code",code);
    //解决outputStream异常的问题
    out.clear();
    pageContext.pushBody();
%>
</body>
</html>
