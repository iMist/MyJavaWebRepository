<%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/14
  Time: 下午11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="js/jquery-1.4.2.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#changeCode").click(function () {

                $(this).attr("src", "/kaptcha.jpg")
            });


            $("#login").click(function () {
                $.post("/login",
                    {code: $("#code").val()},

                    function (data) {
                        if (data == "success") {
                            $("#msg").html(data);
                        } else {
                            $("#msg").html(data);
                            $("#code").val("").focus();

                        }
                    }), "post"
            });
        });
    </script>
</head>
<body>
<form action="" method="post">
    验证码：<input type="text" name="code" value="" id="code" maxlength="5" placeholder="请输入验证码"/><br/>
    <p><input type="button" value="提交" name="login" id="login"></p>
    <div id="msg"></div>
    <img id="changeCode" src=<%=request.getContextPath()%>"/kaptcha.jpg"/>

</form>
</body>
</html>
