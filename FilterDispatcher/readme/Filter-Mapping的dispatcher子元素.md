### filter-mapping 的dispatcher子元素的配置对过滤器的影响

*  默认情况下的配置(不配置的情况下就是以request的方式进行访问)，在访问匹配过滤的资源的的时候，成功进入了对应的过滤器的
   doFilter方法，说明这种访问资源的方式可以被过滤器处理

![](images/image1.png)

* include方式 ： 当过滤器urlpattern匹配的资源被直接访问的时候 不执行过滤器的 doFilter（），但是当资源以 include的方式访问的时候
  调用了doFilter（）
![](images/image3.png)
  
* forward方式 ：  当过滤器urlpattern匹配的资源被直接访问的时候 不执行过滤器的doFilter(),但是当资源以 forward 的方式进行访问
  的时候就会调用doFilter（）方法；
![](images/image4.png)

>以上两种方式 分别是以内置对象request访问，和以jsp内置标签进行访问  
 
* Error方式 ： 当过滤器urlpattern匹配的资源 ，被声明式错误访问的时候，会调用doFilter方法 

>声明式错误
![](images/image2.png)
触发方式 
       1. 当声明的错误发生的时候（被动）
       2. 使用response.sendError(errorCode); 主动触发
       

#####> 说明 ：只要符合过滤器匹配条件的资源被Dispatcher声明的方式访问，无论是在jsp页面触发还是通过Servlet访问都会或者是通过别的方式触发都会触发过滤器       

### 多个Dispatcher子元素的情况 

![](images/image5.png) 

* 只要符合过滤器的匹配方式和资源的访问方式，都会触发过滤器的方法