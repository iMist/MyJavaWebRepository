
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%  //以forward方式访问资源
    //request.getRequestDispatcher("/test2.jsp").forward(request,response);
    //发送404的错误码
    response.sendError(404);
%>
<html>
<head>
    <title>Title1</title>
</head>
<body>

//以include方式访问test2.jsp

<%--<jsp:include page="/test2.jsp"></jsp:include>--%>

<h1>Test1</h1>

</body>
</html>
