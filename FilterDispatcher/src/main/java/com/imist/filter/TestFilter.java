package com.imist.filter;

import javax.servlet.*;
import java.io.IOException;

public class TestFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println(">>>>>> TestFilter execute doFilter ... ... ");
        //通知web服务器已经处理完毕进入下一个过滤器链
        filterChain.doFilter(servletRequest,servletResponse);
    }

    public void destroy() {

    }
}
