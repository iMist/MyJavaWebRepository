### SpringAOP
#### 什么是AOP
 * AOP即Aspect Oriented Programing 面向切面编程;
 * AOP采用横向抽取机制，取代了传统纵向集成体系的重复性代码（性能监测，事务管理，安全检查，缓存；
 
##### 方案演进
![](images/image.png)

#### 什么是SpringAop
* Spring AOP使用纯Java实现，不需要专门的编译过程和类加载器。在运行期通过代理方式向目标类织入增强代码


#### AOP相关术语
* JoinPoint（连接点）：所谓连接点就是那些被拦截到的点,在Spring中，这些点指的是方法，因为Spring中只支持方法类型的这些连接点
* Pointcut（切入点）：所谓的切入点指的是我们要对那些JoinPoint进行拦截的定义；
* Advice(通知/增强)：所谓的通知就是指拦截到JoinPoint之后要做的事情就是通知；
>通知分为：前置通知，后置通知，异常通知，最终通知，环绕通知（切面要完成的功能）

* Introduction（引介）：引介是一种特殊通知在不修改类代码的前提下，Introduction可以在运行期为类动态的添加一些方法或者属性；

* Target(目标)：代理（增强）的目标对象；
* Weaving(织入)：是指把增强应用到目标对象来创建新的代理对象的过程；
>Spring 采用动态代理织入，而AspectJ采用编译期织入和类装载期织入；
* Proxy(代理)：一个类被AOP织入增强后，就产生一个结果代理类；
* Aspect(切面)：是切入点和通知（引介的结合)；
###### 图解更清晰哦
![](images/image1.png)


### JDK动态代理
1. 实现`InvocationHandler`接口，处理具体业务逻辑，拦截方法改变原方法的逻辑
```
    public Object createProxy(){
        Object proxy = Proxy.newProxyInstance(userDao.getClass().getClassLoader(),userDao.getClass().getInterfaces(),this);
        return proxy;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //做删除操作之前进行权限校验
        if ("delete".equals(method.getName())){
            System.out.println("权限检查");
            return method.invoke(userDao,args);
        }
        return method.invoke(userDao,args);
    }
```
2.利用对象生成代理对象，然后用代理对象实现业务逻辑
```
        UserDao userDao = new UserDaoImpl();
        //这里要用被代理的对象去创建一个代理对象createProxy()
        UserDao proxy = (UserDao) new MyJDKProxy(userDao).createProxy();
        proxy.delete();
```


### 使用CGLIB生成动态代理(SpringAOP的底层实现方式)；

* 对于不使用接口的业务类，无法使用JDK动态代理；

* CGLIB采用非常底层的字节码技术，可以为一个类创建子类，解决无接口代理的问题

1. 引入Spring四大核心库 `core` `beans` `expression` `context`,Spring 包含了CGLib的jar包;

2. 创建自定义代理类 实现CGLIB的`MethodInterceptor`接口；核心代码如下:
```
    public Object createProxy(){
        // 1.创建核心类
        Enhancer enhancer = new Enhancer();
        // 2.设置父类
        enhancer.setSuperclass(productDao.getClass());
        // 3.设置回调
        enhancer.setCallback(this);
        // 4.生成代理
        Object proxy = enhancer.create();

        return proxy;
    }

    public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if ("delete".equals(method.getName())){
            System.out.println("权限校验");
            return methodProxy.invokeSuper(proxy,args);
        }
        return methodProxy.invokeSuper(proxy,args);
    }
```

### 代理相关
* Spring在运行期间，生成动态代理对象，不需要特殊的编译器；
* SpringAOP的底层就是通过JDK动态代理或者CGLIB动态代理技术为目标对象实现横向织入；
    1.若目标对象实现了若干接口，Spring使用jdk的java.lang.reflect.Proxy类代理；
    2.若是目标没有实现任何接口，Spring使用CGLIB库生成目标对象的子类；
    
#### 注意事项
* 程序中应优先对接口创建代理，便于程序的解耦和维护；
* 标记为final的方法，不能被代理，因为无法进行覆盖；
    * JDK动态代理，是针对接口生成子类，接口中的方法不能使用final进行修饰；
    * CGLIB是针对目标类生成子类，因此类或者方法不能使用final；
* Spring只支持方法的连接点不支持属性的连接点；

### SpringAop增强类型(通知类型)
* AOP联盟为通知Advice定义了`org.aopalliance.aop.Interface.Advice
* Spring 按照通知Advice在目标方法的连接点位置，可以分为五类
  - 前置通知`org.springframework.aop.MethodBeforeAdvice` 在目标方法前进行增强;
  - 后置通知 `org.springframework.aop.AfterReturningAdvice`在目标方法执行后进行增强;
  - 环绕通知 `org.springframework.aop.MethodInterceptor` 在目标方法执行前后进行增强;
  - 异常抛出通知 `org.springframework.aop.ThrowsAdvice` 在方法抛出异常后实施增强;
  - 引介通知 `org.springframework.aop.IntroductionInterceptor` 在目标类中添加一些新的方法和属性;
  
### SpringAOP切面类型   

- Advisor: 代表一般切面，advice本身就是一个切面，对目标的所有方法进行拦截
- PointcutAdvisor: 代表具有切点的切面，可以指定拦截目标类的哪些方法；
- IntroductionAdvisor: 代表引介切面，针对引介通知而使用切面 


### Spring一般切面实现步骤
* 增强所有的方法
1. 导入aop相关jar包；
```xml
        <!--引入aop联盟的包对Aop进行支持-->
        <dependency>
            <groupId>aopalliance</groupId>
            <artifactId>aopalliance</artifactId>
            <version>1.0</version>
        </dependency>
        <!--引入SpringAOP的的包-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <!--Spring测试包，简化测试-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
```

2. 实现SpringAOP 通知的接口,重写相关方法，处理具体业务逻辑
![](images/image3.png)

3. 完成SpringAOP代理的配置
![](images/image2.png)
```xml
    <!--配置目标类-->
    <bean id="studentDao" class="com.imist.aop.advisor.StudentDaoImpl"/>
    <!--前置通知类型-->
    <bean id="myBeforeAdvice" class="com.imist.aop.advisor.MyBeforeAdvice"/>

    <!--Spring的AOP产生代理对象-->
    <bean id="studentDaoProxy" class="org.springframework.aop.framework.ProxyFactoryBean" >
        <!--配置目标类-->
        <property name="target" ref="studentDao"/>
        <!--实现的接口-->
        <property name="proxyInterfaces" value="com.imist.aop.advisor.StudentDao"/>
        <!--采用拦截的名称-->
        <property name="interceptorNames" value="myBeforeAdvice"/>
    </bean>
```
* 其他属性
![](images/image4.png)
4. 注意测试类的实现
```java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class AdvisorDemo {
    //@Resource(name = "studentDao")
    //注入代理对象,而不是原对象
    @Resource(name = "studentDaoProxy")
    private StudentDao studentDao;

    @Test
    public void demo(){
        studentDao.save();
        studentDao.update();
        studentDao.delete();
        studentDao.select();
    }
}
```


### PointcutAdvisor切点切面实现步骤

* 使用普通的advisor作为切面，将对目标类所有方法进行拦截，不够灵活，在实际开发中常采用带有切点的切面

* 常用PointcutAdvisor实现类
    * DefaultPointcutAdvisor 最常用的切面类型，他可以通过任意Pointcut和Advisor组合定义切面
    * JDKRegexpMethodPointcut 构造正则表达式切点；
    
* 其他方式和一般切入点一样，但是切点需要进行单独配置，用Pointcut的具体实现类；
```xml
<!--配置目标类-->
    <bean id="customerDao" class="com.imist.aop.pointcut_advisor.CustomerDao"/>
    <!--配置通知-->
    <bean id="myAroundAdvisor" class="com.imist.aop.pointcut_advisor.MyAroundAdvisor"/>

    <!-- 一般切面使用通知作为切面，因为要对某些方法进行增强就需要配置一个带有切入点的切面-->
    <bean id="myAdvisor" class="org.springframework.aop.support.RegexpMethodPointcutAdvisor">
        <!--pattern中配置的是一个正则表达式，.代表任意字符，*代表任意次数 ,增强所有方法-->
        <!--<property name="pattern" value=".*"/>-->
        <!--只增强包含save的方法-->
        <!--<property name="pattern" value=".*save.*"/>-->
        <!--增强save方法和delete方法 ，多个匹配用patterns，表达式,分割-->
        <property name="patterns" value=".*save.*,.*delete.*"/>
        <property name="advice" ref="myAroundAdvisor"/>
    </bean>

    <!--配置产生代理-->
    <bean id="customerDaoProxy" class="org.springframework.aop.framework.ProxyFactoryBean">
        <property name="target" ref="customerDao"/>
        <!--被代理类不是接口，使用CGLIB代理-->
        <property name="proxyTargetClass" value="true"/>
        <!-- 一般切面这里配置通知名，带切点的切面这里配置为PointcutAdvisor的实现-->
        <property name="interceptorNames" value="myAdvisor"/>
    </bean>
```    

* 另外，需要注意通知需要实现不同的接口环绕通知是aop联盟下的类；注意使用接口`proxyInterfaces`和使用CGLIB`proxyTargetClass`配置属性的差异；


* 解决方案：自动创建代理
    * BeanNameAutoProxyCreator 根据Bean名称创建代理
    * DefaultAdvisorAutoProxyCreator 根据Advisor本身包含的信息创建代理
    * AnnotationAwareAspectJAutoProxyCreator 基于bean中的AspectJ注解进行自动代理；

#### 自动创建代理`BeanNameAutoProxyCreator` 基于Bean名称的自动代理类  
* 若是每个代理都是通过ProxyFactoryBean 织入切面代理，在实际的开发中，非常多的Bean，每个Bean都要配置ProxyFactoryBean开发维护量巨大

  

>缺点：对符合条件的bean的所有方法产生代理，不能适用于某些类的某个方法，
```xml
    <!--配置目标类-->
    <bean id="customerDao" class="com.imist.aop.beanname_autoproxy.CustomerDao"/>
    <!--接口需要配置实现类-->
    <bean id="studentDao" class="com.imist.aop.beanname_autoproxy.StudentDaoImpl"/>

    <!--配置增强-->
    <bean id="myBeforeAdvice" class="com.imist.aop.beanname_autoproxy.MyBeforeAdvice"/>
    <bean id="myAroundAdvice" class="com.imist.aop.beanname_autoproxy.MyAroundAdvisor"/>

    <!--配置基于Bean名称的自动代理-->
    <bean class="org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator">
        <!--配置Bean名称-->
        <property name="beanNames" value="*Dao"></property>
        <!--配置拦截的名称，也就是拦截的方式-->
        <property name="interceptorNames" value="myBeforeAdvice"></property>
    </bean>
```    

#### 基于切面信息创建代理 `DefaultAdvisorAutoProxyCreator`
* 配置了DefaultAdvisorAutoProxyCreator，然后配置切面信息，若是指定包名，那么符号需要转义
```
<!--配置目标类-->
    <bean id="customerDao" class="com.imist.aop.defaultadvisor_autoproxy_creator.CustomerDao"/>
    <!--接口需要配置实现类-->
    <bean id="studentDao" class="com.imist.aop.defaultadvisor_autoproxy_creator.StudentDaoImpl"/>

    <!--配置增强-->
    <bean id="myBeforeAdvice" class="com.imist.aop.defaultadvisor_autoproxy_creator.MyBeforeAdvice"/>
    <bean id="myAroundAdvice" class="com.imist.aop.defaultadvisor_autoproxy_creator.MyAroundAdvisor"/>

    <!--配置切面-->
    <bean id="myAdvisor" class="org.springframework.aop.support.RegexpMethodPointcutAdvisor">
        <!--这里指定或者匹配切面，这里指定包下的save方法，注意符号需要转义-->
        <property name="pattern" value="com\.imist\.aop\.defaultadvisor_autoproxy_creator\.CustomerDao.save"/>
        <!--使用哪一种通知-->
        <property name="advice" ref="myAroundAdvice"/>
    </bean>

    <!--基于切面信息,产生代理-->
   <bean class="org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator"></bean>
```









    





 