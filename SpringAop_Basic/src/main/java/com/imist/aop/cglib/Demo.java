package com.imist.aop.cglib;

import org.junit.Test;

public class Demo {
    @Test
    public void demo(){
        ProductDao productDao = new ProductDao();
        //这里记得调用创建代理类的方法
        ProductDao proxy = (ProductDao) new MyCGLIBProxy(productDao).createProxy();
        proxy.save();
        proxy.update();
        proxy.delete();
        proxy.select();
    }
}
