package com.imist.aop.pointcut_advisor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext2.xml")
public class SpringDemo {

    @Resource(name = "customerDaoProxy")
    private CustomerDao customerDao;

    @Test
    public void demo(){
        customerDao.save();
        customerDao.delete();
        customerDao.select();
        customerDao.update();

    }
}
