package com.imist.aop.beanname_autoproxy;

import java.util.List;

public class CustomerDao {
    public void save() {
        System.out.println("保存客户...");
    }

    public void update() {
        System.out.println("更新客户...");
    }

    public void delete() {
        System.out.println("删除客户...");
    }

    public List select() {
        System.out.println("查询客户...");
        return null;
    }
}
