package com.imist.aop.beanname_autoproxy;

import java.util.List;

public interface StudentDao {
    public void save();

    public void update();

    public void delete();

    public List select();
}
