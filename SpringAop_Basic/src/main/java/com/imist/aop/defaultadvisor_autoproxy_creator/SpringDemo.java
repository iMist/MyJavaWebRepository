package com.imist.aop.defaultadvisor_autoproxy_creator;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext4.xml")
public class SpringDemo {
    @Resource(name = "studentDao")
    private StudentDao studentDao;
    @Resource(name = "customerDao")
    private CustomerDao customerDao;

    @Test
    public void demo(){
        studentDao.save();
        studentDao.delete();
        studentDao.select();
        studentDao.update();

        customerDao.save();
        customerDao.delete();
        customerDao.select();
        customerDao.update();
    }

}
