package com.imist.aop.defaultadvisor_autoproxy_creator;

import java.util.List;

public interface StudentDao {
    public void save();

    public void update();

    public void delete();

    public List select();
}
