package com.imist.aop.defaultadvisor_autoproxy_creator;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * 环绕增强实现需要实现aop联盟的MethodInterceptor接口
 * 环绕通知非常强大可以阻止目标方法执行
 */
public class MyAroundAdvisor implements MethodInterceptor {

    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        System.out.println("环绕前增强...");
        Object object = methodInvocation.proceed();
        System.out.println("环绕后增强...");
        return object;
    }
}
