package com.imist.aop.advisor;

import java.util.List;

public class StudentDaoImpl implements StudentDao {
    public void save() {
        System.out.println("保存学生...");
    }

    public void update() {
        System.out.println("更新学生...");
    }

    public void delete() {
        System.out.println("删除学生...");
    }

    public List select() {
        System.out.println("查询学生...");
        return null;
    }
}
