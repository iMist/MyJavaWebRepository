package com.imist.aop.advisor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class AdvisorDemo {

    //@Resource(name = "studentDao")
    //注入代理类
    @Resource(name = "studentDaoProxy")
    private StudentDao studentDao;

    @Test
    public void demo(){
        studentDao.save();
        studentDao.update();
        studentDao.delete();
        studentDao.select();
    }
}
