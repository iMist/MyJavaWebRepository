package com.imist.aop.jdkproxy;

import org.junit.Test;

public class SpringDemo1 {
    @Test
    public void demo(){
        UserDao userDao = new UserDaoImpl();
        //这里要用被代理的对象去创建一个代理对象createProxy()
        //java.lang.ClassCastException: MyJDKProxy cannot be cast to UserDao
        UserDao proxy = (UserDao) new MyJDKProxy(userDao).createProxy();
        proxy.update();
        proxy.delete();
        proxy.update();
        proxy.select();
    }
}
