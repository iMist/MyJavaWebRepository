package com.imist.aop.jdkproxy;

import java.util.List;

public interface UserDao {

    public void save();

    public void update();

    public void delete();

    public List select();
}
