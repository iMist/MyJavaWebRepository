package com.imist.aop.jdkproxy;

import java.util.List;

public class UserDaoImpl implements UserDao {
    public void save() {
        System.out.println("保存用户");
    }

    public void update() {
        System.out.println("更新用户");
    }

    public void delete() {
        System.out.println("删除用户");
    }

    public List select() {
        System.out.println("查询用户");
        return null;
    }
}
