package com.imist.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MySecondListener2 implements ServletContextListener {
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("contextInitialized2");
    }

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("contextDestroyed2");
    }
}
