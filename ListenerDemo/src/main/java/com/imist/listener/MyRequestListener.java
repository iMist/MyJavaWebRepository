package com.imist.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

/**
 * 每当用户请求都会进入这监听器的init回调方法 请求结束的时候调用销毁方法
 */
public class MyRequestListener implements ServletRequestListener {

    //这里可以完成请求相关的日志记录
    public void requestInitialized(ServletRequestEvent sre) {
        //注意使用request或者response的时候需要进行强转成HttpServletRequest/HttpServletResponse
        HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
        String path = request.getRequestURI();
        String par = request.getParameter("par");
        System.out.println("requestInitialized : "+path + " --- " +par );
    }

    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.println("requestDestroyed ");
    }


}
