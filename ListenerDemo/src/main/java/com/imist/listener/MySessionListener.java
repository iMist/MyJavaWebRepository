package com.imist.listener;


import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Date;

/**
 * 当用户浏览器访问的时候进行会话创建, 每当一个新的浏览器或者原先的session不在有效期访问都会创建session
 */
public class MySessionListener implements HttpSessionListener {
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("sessionCreated");
        String sessionId = se.getSession().getId();
        long time = se.getSession().getCreationTime();
        Date createTime = new Date(time);
        System.out.println("sessionCreated"+ sessionId + "----" + createTime);
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        System.out.println("sessionDestroyed");

    }
}
