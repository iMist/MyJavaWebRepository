package com.imist.listener;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 * 比较特殊 ，不需要注册，对session的绑定对象进行操作,
 *
 * 将需要进行监听的对象实现其HttpSessionBindingListener
 *
 * 当实现了HttpSessionBindingListener 接口的对象在session中绑定/解绑的时候触发，
 * 通俗点 就是当前对象的示例作为属性在session中被赋值或者被移除的时候触发
 */
public class MyHttpSessionBindingListener implements HttpSessionBindingListener {

    private String username;
    private String password;

    public void valueBound(HttpSessionBindingEvent event) {
        String name = event.getName();
        System.out.println("valueBound : " +name);
    }

    public void valueUnbound(HttpSessionBindingEvent event) {
        String name = event.getName();
        System.out.println("valueUnbound : " +name);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
