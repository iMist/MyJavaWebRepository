package com.imist.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        String appName = context.getInitParameter("AppName");
        String version = context.getInitParameter("Version");
        //将初始化参数放入application域的属性中
        context.setAttribute("AppName",appName);
        context.setAttribute("Version",version);
        //此时就可以在application域中获取对应的属性
        System.out.println("contextInitialized:"+appName +" ---- " + version);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        //在这里可以做一些资源的释放操作, 此方法执行的时候参数一般会自动进行销毁
        ServletContext context = sce.getServletContext();
        String appName = context.getInitParameter("AppName");
        String version = context.getInitParameter("Version");
        //将初始化参数放入application域的属性中
        context.setAttribute("AppName",appName);
        context.setAttribute("Version",version);
        System.out.println("contextDestroyed :"+appName +" ---- " + version);
    }
}
