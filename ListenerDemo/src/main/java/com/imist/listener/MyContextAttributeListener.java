package com.imist.listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;

/**
 * 这里以ServletContextAttributeListener ，另外Session和Request域的属性监听是类似的
 *
 */
public class MyContextAttributeListener implements ServletContextAttributeListener {

    public void attributeAdded(ServletContextAttributeEvent scae) {
        System.out.println("attributeAdded:" +scae.getName() + " -- " +scae.getValue());
    }

    public void attributeRemoved(ServletContextAttributeEvent scae) {
        System.out.println("attributeRemoved:" +scae.getName() + " -- " +scae.getValue());
    }

    public void attributeReplaced(ServletContextAttributeEvent scae) {
        System.out.println("attributeReplaced:"+scae.getName() + " -- " +scae.getValue() );
    }
}
