package com.imist.sm.dao;

import com.imist.sm.entity.Department;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("departmentDao")//这里居然不谢也没有关系?但是还是得补上
public interface DepartmentDao {

    void insert(Department department);

    void delete (Integer id);

    void update(Department department);

    Department selectById(Integer id);

    List<Department> selectAll();
}
