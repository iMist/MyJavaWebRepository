package com.imist.sm.service.impl;

import com.imist.sm.dao.SelfDao;
import com.imist.sm.dao.StaffDao;
import com.imist.sm.entity.Staff;
import com.imist.sm.service.SelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 这里处理业务逻辑，持久层和业务层并不是一一对应的
 */
@Service("selfService")
public class SelfServiceImpl implements SelfService {
    @Autowired
    private SelfDao selfDao;

    @Autowired
    private StaffDao staffDao;

    /**
     * 登陆只需要传入账户查询出用户，防止sql注入
     * @param account
     * @param password
     * @return
     */
    public Staff login(String account, String password) {
        Staff staff = selfDao.selectByAccount(account);
        if (staff == null) return null;
        if (staff.getPassword().equals(password)){
            return staff;
        }
        return null;
    }

    public void changePassword(Integer id, String password) {
        Staff staff = staffDao.selectById(id);
        staff.setPassword(password);
        staffDao.update(staff);
    }
}
