package com.imist.sm.dao;

import com.imist.sm.entity.Staff;
import org.springframework.stereotype.Repository;

@Repository("selfDao")
public interface SelfDao {
    //登陆的操作
    Staff selectByAccount(String account);
}
