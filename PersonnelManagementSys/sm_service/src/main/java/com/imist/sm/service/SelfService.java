package com.imist.sm.service;

import com.imist.sm.entity.Staff;

public interface SelfService {
    Staff login(String account, String password);

    void changePassword(Integer id, String password);

}
