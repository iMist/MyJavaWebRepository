### 本项目是慕课网SM整合开发人员管理系统


### 为什么需要核心控制器
* 因为Servlet由web容器管理，而Services对象由IOC容器管理，而二者是冲突的，所以需要有核心控制器拦截所有的请求，然后交给IOC容器中的service进行管理
![](images/image1.png)

## 关于request的访问路径书写,注意重定向不能传值，只能用request
* 重定向不能传值，使用response发起
```
        //request.getRequestDispatcher("../department_list.jsp") 获取当前访问路径的上一级路径
        request.getRequestDispatcher("../department_list.jsp").forward(request,response);
        //request.getContextPath() 获取当前项目的根访问路径
        //request.getRequestDispatcher(request.getContextPath()+"/department_list.jsp").forward(request,response);
        //  request.getServletPath() 获取当前request的访问路径
        //request.getRequestDispatcher(request.getServletPath()+"/department_list.jsp").forward(request,response);

```

### 返回操作
* `<a href="javascript:history.go(-1);" class="back">返回</a>`

### jstl表达式的的写法，请求地址拼接
 - 正确写法 `<a href="toEdit.do?id=${dep.id}" class="btn">编辑</a>`
 - 错误写法 请求地址为 /toEdit.do?id="" 获取不到id，因为需要在""内部使用作为属性的值而不是拼接在外面 `<a href="toEdit.do?id="${dep.id} class="btn">编辑</a>`
 
### MySql 连接错误导致执行异常，修改连接的时区参数解决问题 
* 解决方案: `"jdbc:mysql://localhost:3306/sm?useSSL=false&amp;useUnicode=true&amp;characterEncoding=utf-8&amp;serverTimezone=UTC"`
* 异常代码
```
Caused by: com.mysql.cj.exceptions.InvalidConnectionAttributeException: The server time zone value '???ú±ê×??±??' is unrecognized or represents more than one time zone. You must configure either the server or JDBC driver (via the serverTimezone configuration property) to use a more specifc time zone value if you want to utilize time zone support.
	at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
	at com.mysql.cj.exceptions.ExceptionFactory.createException(ExceptionFactory.java:59)
```

### 注意时间格式化的对应关系
*  1992-01-04 解析方式 `bornDate = new SimpleDateFormat("yyyy-MM-dd").parse(bornStr);`


### 整合代码实现步骤
* 整合相关包，完成配置文件编写
* 编写dao层接口
* 在资源文件中使用和dao层一致的包名，和同类名的xml文件，完成映射配置和数据处理的配置
* 处理业务层和控制层;


### 注意项目中使用AOP实现日志记录的实现方式