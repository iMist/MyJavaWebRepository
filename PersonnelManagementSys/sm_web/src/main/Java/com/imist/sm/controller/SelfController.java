package com.imist.sm.controller;

import com.imist.sm.entity.Staff;
import com.imist.sm.service.SelfService;
import com.sun.deploy.net.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller("selfController")
public class SelfController {

    @Autowired
    private SelfService selfService;

    //  /toLogin.do 根目录下的toLogin.do,
    public void toLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //访问对应根目录下的页面
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    // /login.do
    public void login (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        Staff staff = selfService.login(account,password);
        if (staff == null){
            response.sendRedirect("toLogin.do");
        }else {
            HttpSession session = request.getSession();
            session.setAttribute("USER",staff);
            response.sendRedirect("main.do");
        }
    }
    public void logout (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        session.removeAttribute("USER");
        // 或者 session.setAttribute("USER",null);
        response.sendRedirect("toLogin.do");
    }

    // /main.do
    public void main(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    // /self/info.do
    public void info(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../info.jsp").forward(request, response);
    }
    //      /self/toChangePassword.do
    public void toChangePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../change_password.jsp").forward(request,response);
    }

    // /self/changePassword.do
    public void changePassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取当前session域中的 Staff
        Staff staff = (Staff) request.getSession().getAttribute("USER");

        String oldPassword = request.getParameter("password");
        String newPassword = request.getParameter("password1");
        if (!staff.getPassword().equals(oldPassword)){
            response.sendRedirect("toChangePassword.do");
        }else {
            //执行修改密码的逻辑
            selfService.changePassword(staff.getId(),newPassword);
            //修改密码之后跳转到登陆界面需要清除session，所以这里直接跳转到logout
            //response.sendRedirect("../logout.do");
            // 直接重定向会在窗口显示，注入脚本，全局显示
            response.getWriter().print("<script type=\"text/javascript\">parent.location.href=\"../logout.do\"</script>");
        }

    }
}
