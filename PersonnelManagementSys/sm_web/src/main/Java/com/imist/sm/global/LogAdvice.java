package com.imist.sm.global;

import com.imist.sm.entity.Log;
import com.imist.sm.entity.Staff;
import com.imist.sm.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
@Aspect
public class LogAdvice {
    @Autowired
    private LogService logService;

    /**
     * 增强操作，进行操作的日志记录
     * 为目标织入After通知 屏蔽掉SelfController和以to开头的方法（防止记录两次辅助页面跳转）
     * @param joinPoint 自动注入的参数 包含了切点信息
     */
    @After("execution(* com.imist.sm.controller.*.*(..)) && !execution(* com.imist.sm.controller.SelfController.*(..)) && !execution(* com.imist.sm.controller.*.to*(..))")
    public void operationLog(JoinPoint joinPoint){
        Log log = new Log();
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        log.setOperation(joinPoint.getSignature().getName());
        HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[0];
        HttpSession session = request.getSession();
        Object obj = session.getAttribute("USER");
        Staff staff = (Staff) obj;
        log.setOperator(staff.getName());
        log.setResult("成功");
        logService.addOperationLog(log);
    }


    /**
     * 异常通知记录异常信息
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(throwing = "e" ,pointcut = "execution(* com.imist.sm.controller.*.*(..)) && !execution(* com.imist.sm.controller.SelfController.*(..)) ")
    public void systemLog(JoinPoint joinPoint,Throwable e){
        Log log = new Log();
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        log.setOperation(joinPoint.getSignature().getName());
        HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[0];
        HttpSession session = request.getSession();
        Object obj = session.getAttribute("USER");
        Staff staff = (Staff) obj;
        log.setOperator(staff.getName());
        log.setResult(e.getClass().getSimpleName());
        logService.addSystemLog(log);
    }

    /**
     * 登录通知
     * @param joinPoint
     */
    @After("execution(* com.imist.sm.controller.SelfController.login(..))")
    public void loginLog(JoinPoint joinPoint){
        log(joinPoint);
    }

    /**
     * 退出通知
     * @param joinPoint
     */
    @Before("execution(* com.imist.sm.controller.SelfController.logout(..))")
    public void logoutLog(JoinPoint joinPoint){
        // 退出的失败结果逻辑上,永远不会执行 ,因为账号永不为null
        log(joinPoint);
    }

    private void log(JoinPoint joinPoint){
        Log log = new Log();
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        log.setOperation(joinPoint.getSignature().getName());
        HttpServletRequest request = (HttpServletRequest) joinPoint.getArgs()[0];
        HttpSession session = request.getSession();
        Object obj = session.getAttribute("USER");
        if (obj == null) {
            log.setOperator(request.getParameter("account"));
            log.setResult("失败");
        }else {
            Staff staff = (Staff) obj;
            log.setOperator(staff.getName());
            log.setResult("成功");
        }
        logService.addLoginLog(log);
    }
}
