* 执行指定路径下的SQL脚本`mysql -uroot -p123456 <[path].sql`，但是这只是执行，还是其他操作还是需要验证的

#### MySQL数据库完成建表之后无法插入中文数据，需要修改表和字段编码格式的情况
* ALTER TABLE `table` DEFAULT CHARACTER SET utf8;虽然修改了表的编码格式，但是字段的编码格式并没有修改过来
* ALTER TABLE `tablename` CHANGE `字段名1` `字段名2` VARCHAR(36) CHARACTER SET utf8 NOT NULL; 一次修改一个字段，需要修改的字段过多时麻烦
  > 使用案例`ALTER TABLE products CHANGE  status status VARCHAR(20) CHARACTER SET utf8 NOT  NULL ;`
* ALTER TABLE `tablename` convert to character set utf8; 可以修改一张表所有字段的编码格式
  >实际使用中会受到外键的影响导致无法修改

#### 什么是事务
* 事务一般特指数据库事务（database transaction）,是指程序执行单元执行的一系列操作，要么完全执行，要么完全不执行；

#### 事务的特性（ACID）
* 原子性（atomicity） 一个事务是一个不可分割的工作单位；结构上保证事务只有两种状态
* 一致性(consistency) 事务必须使数据库从一个一致性状态变到另一个一致性状态；业务上保证事务是合理的
* 隔离性（isolation） 一个事务的执行不能被其他事务干扰；并发的时候两个事物之间不互相干扰
* 持久性（durability） 一个事务一旦提交，它对数据库中数据的改变应该就是永久性的；


#### MySQL事务处理
* 基本规则
  * MySQL只有使用了Innodb数据库引擎的数据库或表才支持事务 
     - show engines;查看服务器支持的引擎
     - MySQL默认以自动提交（autocommit）模式运行；
      
* 语句
    - Begin(start transaction) 显示地开启一个事务
 
    - Commit 提交事务，并使已对数据库进行的所有修改变为永久性的 
 
    - Rollback 回滚事务，并撤销正在进行的所有未提交的修改； 
    
#### 事务并发问题
1. 脏读 : 因为读取了内存中的数据 ![](images/image1.png)
* 独立的session互不干扰，相当于只能读取永久性数据

2. 不可重复读:读取同一条记录项，但是读取的结果不一样；由于另一个事务修改对本事务造成的影响 ![](images/image2.png)
    * 通过锁行的方式解决
3. 幻读 : 已经改过的数据，读取不和逻辑；因为另一个事务执行了插入，对本事务造成了影响， 行级锁无法解决这个问题(因为是新增的数据) 
   ![](images/image3.png)
   * 只能通过锁表的方式解决
          
#### MySQL事务隔离级别，（级别越高，效率越低）
![](images/image4.png)
* `select @@tx_isolation;` 查询默认的隔离级别
* `set session transaction  isolation level XXX;` 设置当前会话隔离级别
    - 使用： `set session transaction  isolation level read uncommitted;`

#### 事务隔离级别测试
```sql
-- 事务隔离级别测试,在不同的会话窗口执行，注意时序图
-- 设置事务隔离级别 读未提交，这状况下，脏读，不可重复读，幻读都存在
set session  transaction isolation level read uncommitted ;

-- 脏读
-- 事务A
begin;
select stock from products  where id = '100001';
rollback ;
-- 事务B
begin ;
update products set stock = 0 where id = '100001'
rollback ;

-- 不可重复读
-- 事务A
begin ;
select stock from products where id = '100001';
select stock from products where id = '100001';
rollback ;
-- 事务B
begin ;
update products set stock = 100 where id = '100001';
commit ;

-- 幻读
-- 事务A
begin ;
update products set stock = 0 ;
select * from products;
rollback ;
-- 事务B
begin ;
insert into products values ('100005','',1999,100,'正常');
commit ;

```
###  JDBC事务处理
* Connection接口:JDBC事务处理是基于Connection的，JDBC通过Connection对象进行事务管理
* jdbc默认事务处理行为是自动提交

* JDBC事务相关方法
    * setAutoCommit 设置自动提交
    * commit  提交
    * rollback 回滚
    
* JDBC事务隔离级别
![](images/image5.png)    
 ```
    /**
    jdbc 的SQL语句执行默认是自动提交的，也就是一句SQL在一个单独的事务中执行
    */
    public void addOrder(){
        try{
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url,username,password);
            //取消自动提交，在该语句和commit或者rollback执行之前的SQL被封装在一个事务当中
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute("insert  into orders values('100001','100001',2,2499,now(),null ,null, '李四','15623151205','武汉','代发货')");
            statement.execute("update  products set stock = stock-2 where id = '100001'");
            statement.close();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
```   
    



## Spring 事务管理
#### Spring 事务处理api
![](images/image6.png)

#### TransationDefinition 接口
![](images/image8.png)

#### Spring 事务传播行为

![](images/image9.png)

## Spring 事务处理方式

#### 基于底层api和TransactionTemplate
![](images/image10.png)

#### 声明式事务处理
![](images/image11.png)
#### 声明式事务处理的分类
![](images/image12.png)

### 基于tx命名空间和基于注解的方式
>基于命名空间的方式在业务层代码方面无需做任何事务性操作，包括注解；注解方式配置少，在量少的时候比较精简，但是随着业务方法的
增多，注解也会越来越多
* 这里只贴最好的方式
```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd
    http://www.springframework.org/schema/context
    http://www.springframework.org/schema/context/spring-context.xsd
    http://www.springframework.org/schema/aop
    http://www.springframework.org/schema/aop/spring-aop.xsd
    http://www.springframework.org/schema/tx
    http://www.springframework.org/schema/tx/spring-tx.xsd">

    <!--基于tx命名空间的事务管理-->

    <!--引入公共的配置-->
    <import resource="spring-dao.xml"/>

    <context:component-scan base-package="com.imist.service.impl"/>

    <!--无论是基于那种实现的事务，都需要注入transactionManager-->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"/>
    </bean>
    <!--设置通知，设置要增强的功能-->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <!--匹配要增强的方法，声明事务传播行为-->
            <tx:method name="get*" propagation="REQUIRED" read-only="true"/>
            <tx:method name="find*" propagation="REQUIRED" read-only="true"/>
            <tx:method name="search*" propagation="REQUIRED" read-only="true"/>
            <!--剩下的方法全部强制事务的方式执行，并且不是只读-->
            <tx:method name="*" propagation="REQUIRED" />
        </tx:attributes>
    </tx:advice>
    <!--配置切面 ,在那些地方增强-->
    <aop:config>
        <!--配置切面，匹配指定包下的所有类的所有方法匹配任意参数-->
        <aop:pointcut id="pointcut" expression="execution(* com.imist.service.impl.*.*(..))"/>
        <!--关联通知，关联切入点-->
        <aop:advisor advice-ref="txAdvice" pointcut-ref="pointcut"/>
    </aop:config>

</beans>
```








