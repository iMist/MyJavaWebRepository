package com.imist.dao;

import com.imist.entity.Product;

import java.util.List;

public interface ProductDao {
    void insert(Product order);
    void update(Product order);
    void delete(String id);
    Product select(String id);
    List<Product> select();

}
