package com.imist.service.impl3;

import com.imist.dao.OrderDao;
import com.imist.dao.ProductDao;
import com.imist.entity.Order;
import com.imist.entity.Product;
import com.imist.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 基于注解的事务处理方式
 * 声明式事务处理，都不需要改变业务方法，实现事务管理；
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private OrderDao orderDao;

    /**
     * 设置强制事务处理，也就是默认的，不设置也是没有关系的
     * 仅包含业务处理逻辑
     * @param order
     */
    //@Transactional
    @Transactional(propagation = Propagation.REQUIRED)
    public void addOrder(Order order) {
        //设置基本的业务规则
        order.setCreateTime(new Date());
        order.setStatus("待付款");

        orderDao.insert(order);
        Product product = productDao.select(order.getProductsId());
        //设置库存
        product.setStock(product.getStock() - order.getNumber());
        productDao.update(product);
    }
}
