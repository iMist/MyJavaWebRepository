package com.imist.service.impl1;

import com.imist.dao.OrderDao;
import com.imist.dao.ProductDao;
import com.imist.entity.Order;
import com.imist.entity.Product;
import com.imist.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import java.util.Date;

/**
 * 基于底层api的事务处理
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private TransactionDefinition transactionDefinition;

    public void addOrder(Order order) {
        //设置基本的业务规则
        order.setCreateTime(new Date());
        order.setStatus("待付款");

        //开启事务
        TransactionStatus transactionStatus = transactionManager.getTransaction(transactionDefinition);

        try{
            orderDao.insert(order);
            Product product = productDao.select(order.getProductsId());
            //设置库存
            product.setStock(product.getStock() - order.getNumber());
            productDao.update(product);
            transactionManager.commit(transactionStatus);
        }catch (Exception e){
            transactionManager.rollback(transactionStatus);
        }
    }
}
