package com.imist.service;

import com.imist.entity.Order;

public interface OrderService {
    void addOrder(Order order);
}
