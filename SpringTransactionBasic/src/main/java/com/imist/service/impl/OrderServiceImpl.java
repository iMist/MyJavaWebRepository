package com.imist.service.impl;

import com.imist.dao.OrderDao;
import com.imist.dao.ProductDao;
import com.imist.entity.Order;
import com.imist.entity.Product;
import com.imist.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import java.util.Date;

/**
 *声明式事务处理，都不需要改变业务方法，实现事务管理；
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private OrderDao orderDao;

    /**
     * 仅包含业务处理逻辑
     * @param order
     */
    public void addOrder(Order order) {
        //设置基本的业务规则
        order.setCreateTime(new Date());
        order.setStatus("待付款");

        orderDao.insert(order);
        Product product = productDao.select(order.getProductsId());
        //设置库存
        product.setStock(product.getStock() - order.getNumber());
        productDao.update(product);
    }
}
