package com.imist.service;

import com.imist.entity.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-service6.xml")
public class OrderTest {
    @Autowired
    private OrderService orderService;

    /**
     * 这里一定要注意 实体类和数据库字段的对应关系通过RowMapper来完成映射
     */
    @Test
    public void addOrder(){
        Order order = new Order("100011","100002",2,1799,"iMist","15623151204","wuhan");
        orderService.addOrder(order);
    }
}
