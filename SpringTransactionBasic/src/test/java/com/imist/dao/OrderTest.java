package com.imist.dao;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class OrderTest {
    private String driver = "com.mysql.cj.jdbc.Driver";
    private String url = "jdbc:mysql://localhost:3306/os?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private String username = "root";
    private String password = "123456";

    /**
     * jdbc 事务基本操作
     */
    @Test
    public void addOrder(){
        try{
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url,username,password);
            //取消自动提交，在该语句和commit或者rollback执行之前的SQL被封装在一个事务当中
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.execute("insert  into orders values('100001','100001',2,2499,now(),null ,null, '李四','15623151205','武汉','代发货')");
            statement.execute("update  products set stock = stock-2 where id = '100001'");
            statement.close();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
