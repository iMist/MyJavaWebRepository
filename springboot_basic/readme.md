1. 导入相关依赖
```
<parent>
        <!--引入Springboot基础组件-->
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.1.RELEASE</version>
    </parent>
    <dependencies>
        <!--SpringBoot启动器的Web支持-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <!--已在parent引入了版本-->
        </dependency>
    </dependencies>
    <build>
        <plugins>
            <plugin>
                <!--打包时将类和资源（包括相关依赖）打包成独立可运行的jar包-->
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
```
![](images/image1.png)

2. 创建约定俗成的文件夹和文件，千万不可以更改
![](images/image2.png)

3. 创建SpringBoot的入口类 `MySpringBootApplication`名字一定要一致
```java
//说明这是一个SpringBoot应用的入口类
@SpringBootApplication
public class MySpringBootApplication {
    public static void main(String[] args) {
        //启动SpringBoot应用
        SpringApplication.run(MySpringBootApplication.class);
    }
}
```
* 项目启动成功
![](images/image3.png)
![](images/image4.png)

4. 浏览器输入地址 `localhost:8080`(默认地址端口8080)
![](images/image5.png)
