package com.imisty.myspringboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * SpringBoot的开发方式和Spring MVC类似
 */
@Controller
public class MyController {

    @RequestMapping("/out")
    @ResponseBody
    public String out(){
        return "success";
    }
}
