
### SpringMVC 拦截器和过滤器的区别;
1. 拦截器是使用JDK动态代理实现的，拦截的是对应调用方法的拦截
2. 过滤器是使用Filter实现的，拦截的是request对象；

### SpringMVC 拦截器的配置和应用
* 相关推荐 ：[SpringMVC拦截器不拦截静态资源的三种处理方式方法](SpringMVC拦截器不拦截静态资源的三种处理方式方法.md)

* SpringMVC拦截器就是对路由进行拦截处理
![](images/image6.png)
![](images/image7.png)

1. 注册并且配置拦截器,注意命名空间部分
```xml
    <!--配置自定义扫描包-->
    <context:component-scan base-package="com.imist"></context:component-scan>

    <!--拦截器的注册 需要先引入mvc的命名空间和schemaLocation-->
    <mvc:interceptors>
        <!--为请求配置拦截器-->
        <mvc:interceptor>
            <!--<mvc:mapping path="/user/search"/>
            <mvc:mapping path="/user/updatePwd"/>
            <mvc:mapping path="/user/updateHeadPic"/>-->
            <!--<mvc:mapping path="/user/*"/> 匹配一级路径 /user/updatePwd/{id}这种路径就无法拦截-->
            <mvc:mapping path="/user/**"/>
            <!--exclude-mapping 在所有拦截中进行排除，一般在通配符会有意义-->
            <mvc:exclude-mapping path="/user/updatePwd"></mvc:exclude-mapping>
            <!--这里多级路径通配符也是有效的-->
            <mvc:exclude-mapping path="/user/show/*"></mvc:exclude-mapping>
            <bean class="com.imist.core.LoginInterceptor"></bean>
        </mvc:interceptor>

        <!--第二个拦截器，形成拦截器栈，会根据注册的先后顺序进行处理-->
        <mvc:interceptor>
            <mvc:mapping path="/user/**"/>
            <bean class="com.imist.core.LogInterceptor"></bean>
        </mvc:interceptor>
    </mvc:interceptors>
```
2.实现Spring框架的`HandlerInterceptor`接口，实现拦截器的方法处理具体的业务逻辑

3.拦截器栈的执行过程
![](images/image8.png)

### SpringMVC 配置文件加载的方式
  * 第一种：[servlet-name]-servlet.xml ,比如，springmvc-servlet.xml
  ![](images/image2.png)
  ```
    org.springframework.beans.factory.BeanDefinitionStoreException: IOException parsing XML document from ServletContext resource [/WEB-INF/SpringMVC-servlet.xml]; nested exception is java.io.FileNotFoundException: Could not open ServletContext resource [/WEB-INF/SpringMVC-servlet.xml]
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:344)
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:304)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:181)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:217)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:188)
	org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:125)
```
 > 必须严格按照命名规范和DispatcherServlet的命名`[servlet-name]-servlet.xml` ，方式，不然出现如上错误；
 
  * 第二种：通过命名空间namespace加载配置文件
   ![](images/image3.png)
   ``` 命名空间配置的名称和配置名称不一致，出现如下错误
    org.springframework.beans.factory.BeanDefinitionStoreException: IOException parsing XML document from ServletContext resource [/WEB-INF/mvc-namespace-servlet1.xml]; nested exception is java.io.FileNotFoundException: Could not open ServletContext resource [/WEB-INF/mvc-namespace-servlet1.xml]
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:344)
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:304)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:181)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:217)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:188)
```
 > 前两种必须将配置文件放在WEB-INF目录下
 
  *  第三种：通过contextConfigLocation,可以将配置文件放在resources资源目录
      ![](images/image4.png) 
      
  
#### classpath和classpath*区别： 
  
   * classpath：只会到你的class路径中查找找文件。
   
   * classpath*：不仅包含class路径，还包括jar文件中（class路径）进行查找。
   
   >注意： 用classpath*:需要遍历所有的classpath，所以加载速度是很慢的；因此，在规划的时候，应该尽可能规划好资源文件所在的路径，尽量避免使用classpath*。
   classpath*的使用：
   
   * 当项目中有多个classpath路径，并同时加载多个classpath路径下（此种情况多数不会遇到）的文件，*就发挥了作用，如果不加*，则表示仅仅加载第一个classpath路径。
   
##### 一些使用技巧：
   
   1. 从上面使用的场景看，可以在路径上使用通配符*进行模糊查找。比如：
  
   `<param-value>classpath:applicationContext-*.xml</param-value>  `
   2. "**/"表示的是任意目录；"**/applicationContext-*.xml"表示任意目录下的以"applicationContext-"开头的XML文件。  
   
   3. 程序部署到tomcat后，src目录下的配置文件会和class文件一样，自动copy到应用的WEB-INF/classes目录下；classpath:与classpath*:的区别在于，前者只会从第一个classpath中加载，而 后者会从所有的classpath中加载。
   
   4. 如果要加载的资源，不在当前ClassLoader的路径里，那么用classpath:前缀是找不到的，这种情况下就需要使用classpath*:前缀。
   
   5. 在多个classpath中存在同名资源，都需要加载时，那么用classpath:只会加载第一个，这种情况下也需要用classpath*:前缀。
  

  
##### 模板创建项目导致web.xml配置过低，又没有直接创建新web.xml的方法（其实可以在facets里面创建）
* 可以创建一个模块，选择正确的配置，然后复制一下
![](images/image1.png)


##### 使用SpringMVC但是没有session等Servlet对象，我们需要在开发阶段导入相关jar包，
![](images/image5.png)




