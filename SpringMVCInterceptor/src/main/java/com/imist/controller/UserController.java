package com.imist.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/search")
    public String search(HttpSession session){
        System.out.println("2.业务方法 ： search");
       /*  判断用户是否登录代码重复，为了保证无入侵实现，引入拦截器
       User user = (User) session.getAttribute("session_user");
        if (user == null){
            //return "redirect:login"; 这样会重定向到 http://localhost:8080/user/login
            return "redirect:/login";
        }
        */
        return "/user/search";
    }

    /**
     * 修改密码
     * @return
     */
    @RequestMapping("/updatePwd")
    public String update(HttpSession session){
        System.out.println("2.业务方法 ：updatePwd");
        return "/user/updatePwd";
    }

    /**
     * 修改头像
     * @return
     */
    @RequestMapping("/updateHeadPic")
    public String updateHeadPic(HttpSession session){
        System.out.println("2.业务方法 ：updateHeadPic");
        return "/user/updateHeadPic";
    }

    /**
     * 公共的显示页面，注意路径参数的传递,不接收也没有关系，但是一定要传，不然无法识别请求404
     * @return
     */
    @RequestMapping("/show/{id}")
    public String show(HttpSession session){
        System.out.println("2.业务方法 ：show");
        return "/user/show";
    }
}
