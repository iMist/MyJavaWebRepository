package com.imist.controller;

import com.imist.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
//这里代表请求地址的拼接,返回的地址还是需要配合视图解析器完成并且不能作用与返回路径
public class LoginController {

    /**
     * 登录操作跳转到登录页面
     * @return
     */
    @RequestMapping("/login")
    public String login(){
        System.out.println("login");
        return "/login";
    }

    /**
     * 处理登录操作的逻辑
     * @param account
     * @param password
     * @return
     */
    @RequestMapping("/logined")
    public String logined(@RequestParam("account") String account ,
                          @RequestParam("password") String password,
                          HttpSession session){
        System.out.println("account : "+account + "password : "+ password);
        if ("admin".equals(account) && "123456".equals(password)){
            User user = new User();
            user.setAccount(account);
            user.setPassword(password);
            session.setAttribute("session_user",user);
            //重定向到search请求
            return "redirect:user/search";
        }else {
            //跳转到指定搜索页面，注意路径
            //return "/user/search";
            // 重定向到login请求
            return "redirect:login";
        }

    }
}
