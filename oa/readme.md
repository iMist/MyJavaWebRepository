### 主要技能点
* 模块分层开发
* 声明式事务 ，封装在业务层，粒度比较合理
* SSM整合 
* 不定项表单处理 ,简而言之就是控制器如何接收参数（集合或者定义DTO对象来接收），前端页面如何显示参数以及提交参数的问题（通过`modelAttribute`和 `path`的对应绑定来实现）


#### 关于静态资源的引用问题

* 因为有两个页面的部分是公有的，可能会被其他的页面引用 ，所以为了在其他的页面访问到静态资源，需要将公有的部分的资源引用设计为绝对路径


#### 前台界面传递模型数据，声明表单封装模型数据
![](images/image1.png)

#### `from:input` 的用法
* `path = "address" `  相当于 id和name = address ,另外类型本来就是输入型文本框
* 原本的 class 属性改为 cssClass
![](images/image2.png)

### 注意整合ssm mybatis的xml映射文件和dao层java文件路径一定要一致，不然出现如下错误
* 多级目录的建立方式 如下，和包名的方式区别很大，不然文件名就是 `com.imisty.oa.dao`,而不是路径名

![](images/image3.png)

```
org.apache.catalina.core.StandardWrapperValve.invoke Servlet.service() for servlet [SpringMVC_DispatcherServlet] in context with path [/oa] threw exception [Request processing failed; nested exception is org.apache.ibatis.binding.BindingException: Invalid bound statement (not found): com.imisty.oa.dao.DepartmentDao.selectAll] with root cause
 org.apache.ibatis.binding.BindingException: Invalid bound statement (not found): com.imisty.oa.dao.DepartmentDao.selectAll
	at org.apache.ibatis.binding.MapperMethod$SqlCommand.<init>(MapperMethod.java:227)
	at org.apache.ibatis.binding.MapperMethod.<init>(MapperMethod.java:49)
```

### 关于配置application context 的项目名的问题
* idea 若是配置了类似的项目名，在进行静态资源使用的时候要注意路径，因为改变次项目名会改变相对路径，从而导致资源访问路径错误,本项目中因为大多数使用了相对路径若是配置了application Context 静态资源的过滤条件和页面相对路径需要改变为绝对路径或者加上配置的项目名；
* tomcat在发布项目的时候不考虑用虚拟路径的话，一般都是把项目放在webapps里面，这个时候具体放置的位置就会影响访问路径了。如果是直接放在webapps目录下，那么访问路径就是和eclipse相同的访问路径了，但是这时候如果把项目放在tomcat的webapps里的root文件夹下，那么这个时候这个项目就成为了tomcat的默认项目，直接用localhost:8080就可以访问，安装tomcat时检查tomcat是否正常安装出现的项目其实就是root。 
  
### 数据库编码正常但是filter没有处理字符导致的乱码问题
* 起初以为是数据库的编码问题导致的乱码，结果查询数据库的编码集正常 `show variables like'%char%';`
![](images/image4.png)

* 之后打印了dao层和业务层的实体类出现乱码，就确定了是业务处理乱码或者前端页面的编码问题导致的乱码，检查了前端编码也正常
* 最后在filter 部分打断点，发现根本没有执行，检查拦截的url-pattern，发现和servlet的拦截全部还是有区别的? 改成 `/*` 解决了问题
```
<!--配置编码过滤器-->
  <filter>
    <filter-name>EncodingFilter</filter-name>
    <filter-class>com.imisty.oa.global.EncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>EncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
  ```
  
#### 一个比较标准的数据库连接url
* `<property name="url" value="jdbc:mysql://localhost:3306/oa?useSSL=false&amp;useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=GMT"/>`
  
#### 数据表之间关系的映射配置以及注意事项
* 若是两个表中没有相同名称的属性 ，可以直接使用resultMap建立关联关系，不然就需要在`association`标签中配置一下关联表的映射配置
* 若是关联的表有些字段不需要使用或不关心，可以只配置关注的部分
* 关联表的字段名`column`一定不可以和当前`resultMap`配置的字段名一致一致，
```xml
<!-- 员工与部门之间的对应关系，配置数据库映射文件以及表之间的关联关系-->
    <resultMap id="employee" type="Employee">
        <id property="sn" column="sn" javaType="String"/>
        <result property="password" column="password" javaType="String"/>
        <result property="name" column="name" javaType="String"/>
        <result property="departmentSn" column="department_sn" javaType="String"/>
        <result property="post" column="post" javaType="String"/>

        <!-- 员工与部门之间的对应关系 , 配置属性和外键以及对应的数据类型-->
        <association property="department" column="department_sn" javaType="Department">
            <id property="sn" column="dsn" javaType="String"/>
            <result property="name" column="dname" javaType="String"/>
            <!--<result property="address" column="address" javaType="String"/>-->
        </association>
    </resultMap>
```

### 因为编译器识别到了两个同名的bean，所以无法自动注入，但是检查只有只有一个，莫名其妙；
![](images/image5.png)
* 推荐使用 `@Resource(name = "employeeDao")` 或者 `@Autowired @Qualifier("employeeDao")` 的方式，需要使用高版本的Java，不然无法通过编译
  
 ### `HttpSession` 在SpringMvc控制器层的请求处理逻辑控制器中是直接注入的 ，可以再直接使用
 ```java
    @RequestMapping("/login")
    public String login(HttpSession session , @RequestParam("sn") String sn, @RequestParam String password){
        Employee employee = globalBiz.login(sn,password);
        if (employee == null){
            return "redirect:to_login";
        }
        session.setAttribute("employee",employee);
        return "redirect:self";
    }
```
 
 #### SpringMVC 的标签使用
 * 表单可以提交模型数据，但是需要用到SpringMVC标签封装一下，或者直接注册json转换器使用json数据
 * 若是使用了 `class `属性，在表单元素中要想显示的css样式正常，需要改为 `cssClass`
 * `modelAttribute` 用来绑定视图和模型数据 ，声明的模型数据属性要和标签元素的 `path` 对应，不然无法绑定；
 * 通过`modelAttribute` 和path属性的绑定，减少el表达式的使用简化代码
 ```
        <!-- 使用隐藏域处理密码,需要将密码传到页面，其实这里可以在逻辑处理部分进行控制仅修改信息不修改密码就行-->
        <form:hidden path="password"/>
        <!--form 表单类型的标签 modelAttribute 表单接收的模型数据 -->
     <form:form action="/employee/add" modelAttribute="employee"  id="admin-form" name="addForm">
     </form:form>
     <!-- input类型的标签 path 属性声明id和name -->
    <form:input path="name" class="gui-input" placeholder="姓名..."/>
    
    <!-- select类型的标签 path 属性声明id和class items 集合参数名称,itemLabel 集合元素属性  itemValue 集合元素的唯一标示id-->
    
    <form:select path="departmentSn" items="${dlist}" itemLabel="name" itemValue="sn" cssClass="gui-input" placeholder="所属部门..."/>
     
     <%--这里因为数据类型本就是String类型，而需要展示的数据也是String类型，直接显示即可--%>
     <form:select path="post" items="${plist}" cssClass="gui-input" placeholder="职务..."/>    
 ```
 
 ### 浏览器提示重定向次数过多
 * 是因为本来应该返回页面，但是返回了一个重定向请求，请求自身
 ```jsp
    //跳转到个人信息界面
    @RequestMapping("/self")
    public String self(){
        //这里千万不可以用重定向,不然会导致无限次的请求自身（不是页面而是请求），出现严重逻辑错误，导致浏览器提示处对象次数过多
        //return  "redirect:self";
        return "self";
    }
```

#### mybatis查询语句
  * 自动生成主键值并赋值给对象的指定属性
```
       <!--useGeneratedKeys keyProperty 自动生成ID赋值给 id 属性-->
       <insert id="insert" useGeneratedKeys="true" keyProperty="id" parameterType="ClaimVoucher">
           insert into claim_voucher(cause,create_sn,create_time,next_deal_sn,total_amount,status)
           values(#{cause},#{createSn},#{createTime},#{nextDealSn},#{totalAmount},#{status})
       </insert>
 ```
 
 * 连接查询，因为有两个外键关联同一张表所以连接两次
 ```
    <select id="selectByCreateSn" resultMap="claimVoucher" parameterType="String">
        select cv.*,ce.name cname,ce.post cpost,d.name dname,d.post dpost
        from claim_voucher cv
            left join employee ce on ce.sn=cv.create_sn
            left join employee d on d.sn = cv.next_deal_sn
        where cv.create_sn=#{csn} order by cv.create_time desc
    </select>
```

#### Spring的日期显示的格式化输出
1.在需要显示的日期字段上加上 `@DateTimeFormat` 标签
    ```
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm")
    private Date createTime;
    ```
    
2. 使用标签的`expression`属性进行日期的格式化处理
 ```<div class="col-md-4"><spring:eval expression="claimVoucher.createTime"/></div>```    
 
 #### 集合类型的属性在页面进行显示
 >  我们常使用<c:forEach>标签来遍历需要的数据，为了方便使用，varStatus属性可以方便我们实现一些与行数相关的功能，如：奇数行、偶数行差异；最后一行特殊处理等等。varStatus属性常用参数如下：
    current：当前这次迭代的（集合中的）项;
 index：当前这次迭代从 0 开始的迭代计数;
 count：当前这次迭代从 1 开始的迭代计数;
 first：用来表明当前这轮迭代是否为第一次迭代的标志，返回true/false;
 last：用来表明当前这轮迭代是否为最后一次迭代的标志,返回true/false;
 
*  遍历集合的属性 ,声明集合元素,事实上定义了一个 `sta`名的对象作为varStatus的绑定值。该值封装了当前的遍历对象

   ```
                   <!--代码片段-->
                  <c:forEach items="${info.items}" varStatus="sta">
                   <div>
                       <div class="col-md-3">
                            <label for="items[${sta.index}].item" class="field prepend-icon">
                            <form:hidden path="items[${sta.index}].id"/>
                            <form:hidden path="items[${sta.index}].claimVoucherId"/>
                             <form:select path="items[${sta.index}].item" cssClass="gui-input" placeholder="花销类型..." items="${items}"/>
                     </label>
                  </div>
   ```
   
#### 连同按钮一块提交，将按钮的值以name为key 作为表单元素提交
* `<button type="submit" class="button" name="dealWay" value="${Constant.DEAL_PASS}" >${Constant.DEAL_PASS}</button>`   

#### MVC三层架构
* View 也就是用户界面
* Controller 就是前端控制器处理请求的部分，也就是用户接口
* Model 其他部分基本都可以称之为模型，包括业务模型和数据库模型



