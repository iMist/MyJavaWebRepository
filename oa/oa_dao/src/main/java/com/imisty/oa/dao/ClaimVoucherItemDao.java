package com.imisty.oa.dao;

import com.imisty.oa.entity.ClaimVoucherItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("claimVoucherItemDao")
public interface ClaimVoucherItemDao {

    void insert(ClaimVoucherItem claimVoucherItem);

    void delete(int id);

    void update(ClaimVoucherItem claimVoucherItem);

    List<ClaimVoucherItem> selectByClaimVoucher(int cvid);



}
