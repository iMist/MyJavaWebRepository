package com.imisty.oa.dao;
import com.imisty.oa.entity.DealRecord;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("dealRecordDao")
public interface DealRecordDao {

    void insert(DealRecord dealRecord);

    List<DealRecord> selectByClaimVoucher(int cvid);



}
