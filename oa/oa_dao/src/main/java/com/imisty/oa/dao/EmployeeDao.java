package com.imisty.oa.dao;

import com.imisty.oa.entity.Employee;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("employeeDao")
public interface EmployeeDao {

    void insert(Employee employee);

    void update(Employee employee);

    void delete(String sn);

    Employee select(String sn);

    List<Employee> selectAll();

    //根据部门和职位获取员工(因为不是主键获取，所以这里返回列表，通常情况下只有一个）
    List<Employee> selectByDepartmentAndPost(@Param("dsn") String dsn, @Param("post") String post);


}
