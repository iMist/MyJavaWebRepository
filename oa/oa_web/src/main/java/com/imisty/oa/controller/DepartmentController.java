package com.imisty.oa.controller;


import com.imisty.oa.biz.DepartmentBiz;
import com.imisty.oa.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller("departmentController")
@RequestMapping("/department")
public class DepartmentController {


    @Autowired
    private DepartmentBiz departmentBiz;

    /**
     * 使用map因为是java类型不依赖于spring mvc,方便替换
     * @param map
     * @return
     */
    @RequestMapping("/list")
    public String list(Map<String,Object> map){
        map.put("list",departmentBiz.selectAll());
        return "department_list";
    }

    /**
     * 跳转到添加界面
     * @param map
     * @return
     */
    @RequestMapping("/to_add")
    public String toAdd(Map<String,Object> map){
        map.put("department",new Department());
        return "department_add";
    }

    /**
     * 处理添加请求
     * @param department
     * @return
     */
    @RequestMapping("/add")
    public String add(Department department){
        departmentBiz.add(department);
        //重定向到list 控制器,
        return "redirect:list";
    }

    /**
     * 去更新页面 ，需要强制传递一个sn参数
     * @param sn
     * @param map
     * @return
     */
    @RequestMapping(value = "/to_update",params = "sn")
    public String toUpdate(String sn ,Map<String,Object> map){
        map.put("department",departmentBiz.select(sn));
        return "department_update";
    }

    /**
     * 处理更新请求
     * @param department
     * @return
     */
    @RequestMapping("/update")
    public String update(Department department){
        departmentBiz.edit(department);
        //重定向到list 控制器,
        return "redirect:list";
    }

    /**
     * 删除不需要接收其他的参数，只需要sn
     * @param sn
     * @return
     */
    @RequestMapping(value = "/remove",params = "sn")
    public String remove(String sn){
        departmentBiz.remove(sn);
        //重定向到list 控制器,
        return "redirect:list";
    }
}
