package com.imisty.oa.controller;


import com.imisty.oa.biz.DepartmentBiz;
import com.imisty.oa.biz.EmployeetBiz;
import com.imisty.oa.entity.Employee;
import com.imisty.oa.global.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller("employeeController")
@RequestMapping("/employee")
public class EmployeeController {


    @Autowired
    private EmployeetBiz employeetBiz;
    @Autowired
    private DepartmentBiz  departmentBiz;

    /**
     * 使用map因为是java类型不依赖于spring mvc,方便替换
     * @param map
     * @return
     */
    @RequestMapping(value = "/list")
    public String list(Map<String,Object> map){
        map.put("list",employeetBiz.selectAll());
        return "employee_list";
    }

    /**
     * 跳转到添加界面
     * @param map
     * @return
     */
    @RequestMapping("/to_add")
    public String toAdd(Map<String,Object> map){
        map.put("employee",new Employee());
        map.put("dlist",departmentBiz.selectAll());
        //传入所有职位
        map.put("plist",Constant.getPosts());
        return "employee_add";
    }

    /**
     * 处理添加请求
     * @param employee
     * @return
     */
    @RequestMapping("/add")
    public String add(Employee employee){
        employeetBiz.add(employee);
        //重定向到当前路径的list  /employee/list,
        return "redirect:list";
    }

    /**
     * 去更新页面 ，需要强制传递一个sn参数
     * @param sn
     * @param map
     * @return
     */
    @RequestMapping(value = "/to_update",params = "sn")
    public String toUpdate(String sn ,Map<String,Object> map){
        map.put("employee",employeetBiz.select(sn));
        map.put("dlist",departmentBiz.selectAll());
        //传入所有职位
        map.put("plist",Constant.getPosts());
        return "employee_update";
    }

    /**
     * 处理更新请求
     * @param employee
     * @return
     */
    @RequestMapping("/update")
    public String update(Employee employee){
        employeetBiz.edit(employee);
        //重定向到list 控制器,
        return "redirect:list";
    }

    /**
     * 删除不需要接收其他的参数，只需要sn
     * @param sn
     * @return
     */
    @RequestMapping(value = "/remove",params = "sn")
    public String remove(String sn){
        employeetBiz.remove(sn);
        //重定向到list 控制器,
        return "redirect:list";
    }
}
