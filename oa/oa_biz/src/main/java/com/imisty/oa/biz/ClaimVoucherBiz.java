package com.imisty.oa.biz;

import com.imisty.oa.entity.ClaimVoucher;
import com.imisty.oa.entity.ClaimVoucherItem;
import com.imisty.oa.entity.DealRecord;

import java.util.List;

public interface ClaimVoucherBiz {

    void save(ClaimVoucher claimVoucher , List<ClaimVoucherItem> items);

    ClaimVoucher get(int id);

    List<ClaimVoucherItem> getItems(int cvid);

    List<DealRecord> getRecords(int cvid);

    List<ClaimVoucher> getForSelf(String sn);

    List<ClaimVoucher> getForDeal(String sn);

    void update(ClaimVoucher claimVoucher , List<ClaimVoucherItem> items);

    void submit(int id);

    void deal(DealRecord dealRecord);

}
