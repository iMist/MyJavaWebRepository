package com.imisty.oa.biz.impl;

import com.imisty.oa.biz.EmployeetBiz;
import com.imisty.oa.dao.EmployeeDao;
import com.imisty.oa.entity.Employee;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("employeeBiz")
public class EmployeeBizImpl implements EmployeetBiz {



    @Resource(name = "employeeDao")
    private EmployeeDao employeeDao;

    public void add(Employee employee) {
        // 业务层默认密码为123456
        employee.setPassword("123456");
        employeeDao.insert(employee);
    }

    public void edit(Employee employee) {
        employeeDao.update(employee);
    }

    public void remove(String sn) {
        employeeDao.delete(sn);
    }

    public Employee select(String sn) {
        return employeeDao.select(sn);
    }

    public List<Employee> selectAll() {
        return employeeDao.selectAll();
    }
}
