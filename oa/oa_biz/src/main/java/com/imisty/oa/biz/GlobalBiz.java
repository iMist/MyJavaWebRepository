package com.imisty.oa.biz;

import com.imisty.oa.entity.Employee;

public interface GlobalBiz {

    Employee login(String sn,String password);

    //为了提供的接口更加优秀易用，传入Employee即可，在表现层很容易就得到
    void changePassword(Employee employee);

    //登出操作是对session的操作,在表现层完成即可

    //查看用户信息 是对session内容的查看所以不用操作数据库和业务层

}
