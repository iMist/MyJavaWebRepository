package com.imisty.oa.biz.impl;

import com.imisty.oa.biz.ClaimVoucherBiz;
import com.imisty.oa.dao.ClaimVoucherDao;
import com.imisty.oa.dao.ClaimVoucherItemDao;
import com.imisty.oa.dao.DealRecordDao;
import com.imisty.oa.dao.EmployeeDao;
import com.imisty.oa.entity.ClaimVoucher;
import com.imisty.oa.entity.ClaimVoucherItem;
import com.imisty.oa.entity.DealRecord;
import com.imisty.oa.entity.Employee;
import com.imisty.oa.global.Constant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
@Service("claimVoucherBiz")
public class ClaimVoucherBizImpl implements ClaimVoucherBiz {

    @Resource
    private ClaimVoucherDao claimVoucherDao;

    @Resource
    private ClaimVoucherItemDao claimVoucherItemDao;

    @Resource
    private DealRecordDao dealRecordDao;

    @Resource
    private EmployeeDao employeeDao;

    @Override
    public void save(ClaimVoucher claimVoucher, List<ClaimVoucherItem> items) {
        claimVoucher.setCreateTime(new Date());
        //新创建的报销单待处理人就是自己
        claimVoucher.setNextDealSn(claimVoucher.getCreateSn());
        claimVoucher.setStatus(Constant.CLAIMVOUCHER_CREATE);
        claimVoucherDao.insert(claimVoucher);

        for (ClaimVoucherItem item : items) {
            //其他参数从页面上传递,报销单编号在上个业务方法执行完成后获取
            item.setClaimVoucherId(claimVoucher.getId());
            claimVoucherItemDao.insert(item);
        }

    }

    @Override
    public ClaimVoucher get(int id) {
        return claimVoucherDao.select(id);
    }

    @Override
    public List<ClaimVoucherItem> getItems(int cvid) {
        return claimVoucherItemDao.selectByClaimVoucher(cvid);
    }

    @Override
    public List<DealRecord> getRecords(int cvid) {
        return dealRecordDao.selectByClaimVoucher(cvid);
    }

    @Override
    public List<ClaimVoucher> getForSelf(String sn) {
        return claimVoucherDao.selectByCreateSn(sn);
    }

    @Override
    public List<ClaimVoucher> getForDeal(String sn) {
        return claimVoucherDao.selectByNextDealSn(sn);
    }

    @Override
    public void update(ClaimVoucher claimVoucher, List<ClaimVoucherItem> items) {
        //新创建的报销单待处理人就是自己
        //更新基本信息
        claimVoucher.setNextDealSn(claimVoucher.getCreateSn());
        claimVoucher.setStatus(Constant.CLAIMVOUCHER_CREATE);
        claimVoucherDao.update(claimVoucher);

        //更新条目信息
        List<ClaimVoucherItem> olds = claimVoucherItemDao.selectByClaimVoucher(claimVoucher.getId());
        for (ClaimVoucherItem old : olds) {
            boolean isHave = false;
            for (ClaimVoucherItem item : items) {
                if (item.getId() == old.getId()){
                    isHave = true;
                }
            }
            if (!isHave){
                claimVoucherItemDao.delete(old.getId());
            }
        }
        for (ClaimVoucherItem item : items) {
            item.setClaimVoucherId(claimVoucher.getId());
            if (item.getId() != null && item.getId() > 0){
                claimVoucherItemDao.update(item);
            }else {
                claimVoucherItemDao.insert(item);
            }
        }
    }

    @Override
    public void submit(int id) {
        ClaimVoucher claimVoucher = claimVoucherDao.select(id);
        Employee employee = employeeDao.select(claimVoucher.getCreateSn());

        claimVoucher.setStatus(Constant.CLAIMVOUCHER_SUBMIT);
        //得到当前部门的部门经理,若是没有部门经理则会报错
        claimVoucher.setNextDealSn(employeeDao.selectByDepartmentAndPost(employee.getDepartmentSn(),Constant.POST_FM).get(0).getSn());

        claimVoucherDao.update(claimVoucher);

        DealRecord dealRecord = new DealRecord();
        dealRecord.setDealWay(Constant.DEAL_SUBMIT);
        dealRecord.setDealSn(employee.getSn());
        dealRecord.setClaimVoucherId(id);
        dealRecord.setDealResult(Constant.CLAIMVOUCHER_SUBMIT);
        dealRecord.setDealTime(new Date());
        dealRecord.setComment("无");
        dealRecordDao.insert(dealRecord);
    }

    public void deal(DealRecord dealRecord){
        //根据处理记录ID获取报销单,根据报销单的处理人ID获取处理人信息
        ClaimVoucher claimVoucher = claimVoucherDao.select(dealRecord.getClaimVoucherId());
        Employee employee = employeeDao.select(dealRecord.getDealSn());


        if (dealRecord.getDealWay().equals(Constant.DEAL_PASS)){
            //如果小于限制金额或者处理人是总经理直接进行下一步
            if (claimVoucher.getTotalAmount()<= Constant.LIMIT_CHECK|| employee.getPost().equals(Constant.POST_GM)){
                claimVoucher.setStatus(Constant.CLAIMVOUCHER_APPROVED);
                //交给财务进行下一步处理不需要当前部门，只需要是财务即可
                claimVoucher.setNextDealSn(employeeDao.selectByDepartmentAndPost(null,Constant.POST_CASHIER).get(0).getSn());
                dealRecord.setDealResult(Constant.CLAIMVOUCHER_APPROVED);
            }else{
                //超过金额限制，且处理人不是总经理，需要交给总经理进行处理
                claimVoucher.setStatus(Constant.CLAIMVOUCHER_RECHECK);
                claimVoucher.setNextDealSn(employeeDao.selectByDepartmentAndPost(null,Constant.POST_GM).get(0).getSn());

                dealRecord.setDealResult(Constant.CLAIMVOUCHER_RECHECK);
            }
        }else if (dealRecord.getDealWay().equals(Constant.DEAL_BACK)){
            claimVoucher.setStatus(Constant.CLAIMVOUCHER_BACK);
            claimVoucher.setNextDealSn(claimVoucher.getCreateSn());

            dealRecord.setDealResult(Constant.CLAIMVOUCHER_BACK);
        }else if (dealRecord.getDealWay().equals(Constant.DEAL_REJECT)){
            //被拒绝就直接终止
            claimVoucher.setStatus(Constant.CLAIMVOUCHER_TERMINATED);
            claimVoucher.setNextDealSn(null);

            dealRecord.setDealResult(Constant.CLAIMVOUCHER_TERMINATED);
        }else if (dealRecord.getDealWay().equals(Constant.DEAL_PAID)){
            //完成打款操作也终止
            claimVoucher.setStatus(Constant.CLAIMVOUCHER_PAID);
            claimVoucher.setNextDealSn(null);

            dealRecord.setDealResult(Constant.CLAIMVOUCHER_PAID);
        }
        claimVoucherDao.update(claimVoucher);
        dealRecordDao.insert(dealRecord);

    }
}
