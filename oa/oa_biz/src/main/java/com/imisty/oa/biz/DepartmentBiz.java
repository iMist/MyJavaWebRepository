package com.imisty.oa.biz;

import com.imisty.oa.entity.Department;

import java.util.List;

public interface DepartmentBiz  {

    void add(Department department);

    void edit(Department department);

    void remove(String sn);

    Department select(String sn);

    List<Department> selectAll();
}
