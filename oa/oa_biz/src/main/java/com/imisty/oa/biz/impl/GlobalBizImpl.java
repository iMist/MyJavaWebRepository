package com.imisty.oa.biz.impl;

import com.imisty.oa.biz.GlobalBiz;
import com.imisty.oa.dao.EmployeeDao;
import com.imisty.oa.entity.Employee;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("globalBizImpl")
public class GlobalBizImpl implements GlobalBiz {
    @Resource
    private EmployeeDao employeeDao;

    @Override
    public Employee login(String sn, String password) {
        Employee employee = employeeDao.select(sn);
        //只有当用户存在，且密码和输入的密码相同才算登陆成功，返回用户对象
        if (employee != null && employee.getPassword().equals(password)){
            return employee;
        }
        return null;
    }

    @Override
    public void changePassword(Employee employee) {
        employeeDao.update(employee);
    }
}
