package com.imisty.oa.biz.impl;

import com.imisty.oa.biz.DepartmentBiz;
import com.imisty.oa.dao.DepartmentDao;
import com.imisty.oa.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("departmentBiz")
public class DepartmentBizImpl implements DepartmentBiz {

    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public void add(Department department) {
        departmentDao.insert(department);
    }

    @Override
    public void edit(Department department) {
        departmentDao.update(department);
    }

    @Override
    public void remove(String sn) {
        departmentDao.delete(sn);
    }

    @Override
    public Department select(String sn) {
        return  departmentDao.select(sn);
    }

    @Override
    public List<Department> selectAll() {
        return departmentDao.selectAll();
    }
}
