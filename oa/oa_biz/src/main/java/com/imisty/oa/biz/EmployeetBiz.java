package com.imisty.oa.biz;

import com.imisty.oa.entity.Employee;

import java.util.List;

public interface EmployeetBiz {

    void add(Employee employee);

    void edit(Employee employee);

    void remove(String sn);

    Employee select(String sn);

    List<Employee> selectAll();
}
