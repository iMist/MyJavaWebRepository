package com.imist.aspectJ.demo2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class MyAspectXML {

    /**
     * 前置通知,可以得到相关JoinPoint的信息
     */
    public void before(JoinPoint joinPoint){
        System.out.println("xml方式的前置通知...."+joinPoint);
    }

    /**
     * 后置通知
     */
    public void afterReturning(Object result){
        System.out.println("xml方式的后置通知...."+result);
    }

    /**
     * 环绕通知
     */
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("xml方式的环绕前通知....");
        //执行目标方法
        Object obj = proceedingJoinPoint.proceed();
        System.out.println("xml方式的环绕后通知....");
        return obj;
    }

    /**
     * 异常抛出通知，只有在出现异常的时候才会执行，可以得到异常信息
     */
    public void afterThrowing(Throwable exception){
        System.out.println("xml方式的异常抛出通知"+exception.getMessage());
    }

    /**
     *最终通知，无论是否发生异常都会执行
     */
    public void after()  {
        System.out.println("xml方式的最终通知.....");
    }
}
