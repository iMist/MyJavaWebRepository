package com.imist.aspectJ.demo2;

public class CustomerDaoImpl implements CustomerDao {

    public void save() {
        System.out.println("保存客户....");
    }

    public String delete() {
        System.out.println("删除客户....");
        return "Return : afterReturning....";
    }

    public void update() {
        System.out.println("更新客户信息....");
    }

    public void findOne() {
        System.out.println("查询一个客户....");
        int i = 10/0;
    }

    public void findAll() {
        System.out.println("查询全部客户....");
    }
}
