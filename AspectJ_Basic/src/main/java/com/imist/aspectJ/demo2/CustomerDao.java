package com.imist.aspectJ.demo2;

public interface CustomerDao {
    public void save();
    public String delete();
    public void update();
    public void findOne();
    public void findAll();

}
