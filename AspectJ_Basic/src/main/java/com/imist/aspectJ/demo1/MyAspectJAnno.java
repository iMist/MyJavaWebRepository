package com.imist.aspectJ.demo1;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * 切面类
 */
@Aspect
public class MyAspectJAnno {

    /**
     * 对指定包下指定类的所有方法进行前置增强
     */
    /*@Before(value = "execution(* com.imist.aspectJ.demo1.ProductDao.*(..))")
    public void before(){
        System.out.println("前置通知....");
    }*/

    /**
     * 仅对save方法进行增强
     */
    @Before(value = "pointcut1()")
    public void beforeSave(JoinPoint joinPoint){
        System.out.println("前置通知================"+joinPoint);
    }

    @AfterReturning(value = "pointcut2()" ,returning = "result")
    public void afterReturning(Object result){
        System.out.println("后置通知================"+result);
    }

    @Around(value = "pointcut3()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕前通知=========");
        //这一句不执行那么目标方法会被拦截不会执行
        Object obj = proceedingJoinPoint.proceed();
        System.out.println("环绕后通知=========");
        return obj;
    }

    /**
     * 异常抛出通知,只有当发生异常的时候，异常通知方法才会执行
     * @param exception 异常
     */
    @AfterThrowing(value = "pointcut4())",throwing = "exception")
    public void afterThrowing(Throwable exception){
        System.out.println("异常抛出通知========="+ exception.getMessage());
    }

    @After(value = "pointcut5())")
    public void after(){
        System.out.println("最终通知=========有异常也会被执行");
    }

    /**
     * private  void + 无参数的空方法 来定义切点，方法名为切点名
     * 而不是直接才通知上定义切点，方便维护
     */
    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.save(..))")
    private void pointcut1(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.update(..))")
    private void pointcut2(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.delete(..))")
    private void pointcut3(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.findAll(..))")
    private void pointcut4(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.findOne(..))")
    private void pointcut5(){}


}
