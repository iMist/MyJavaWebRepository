package com.imist.aspectJ.demo1;

public class ProductDao {

    public void save(){
        System.out.println("保存商品....");
    }

    public void delete(){
        System.out.println("删除商品....");
    }

    public String update(){
        System.out.println("修改商品....");
        return "hello";
    }
    public void findAll(){
        System.out.println("查询全部商品....");
        //测试异常
        int i = 10/0;
    }
    public void findOne(){
        System.out.println("查询一个指定商品....");
        int i = 10/0;
    }
}
