## AspectJ简介

* AspectJ是一个基于Java语言的AOP框架；
* Spring2.0 以后新增了对AspectJ切点表达式支持；
* @AspectJ 是AspectJ1.5新增功能，通过JDK5注解技术，允许直接在Bean中定义切面；
* 新版本Spring框架，建议使用AspectJ方式来开发AOP;
* 使用AspectJ需要导入SpringAOP和AspectJ相关jar包(当然也需要引入Spring的核心库)
![](images/image1.png)

### AspectJ的AOP开发之注解开发

#### @AspectJ提供不同的通知类型
* @Before 前置通知，相当于BeforeAdvice;
* @AfterReturning，相当于AfterReturningAdvice;
* @Around 环绕通知，相当于MethodInterceptor
* @AfterThrowing异常抛出通知，相当于ThrowAdvice
* @After最终final通知，不管是否异常，该通知都会执行；
* @DeclareParents 引介通知，相当于IntroductionInterceptor

#### 在通知中通过value属性定义切点；
![](images/image2.png)

1. 引入相关jar包
```xml
      <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>LATEST</version>
            <scope>test</scope>
        </dependency>
        <!--Spring 四大核心库-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-expression</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <!--导入基本的aop开发包，无论是传统的还是基于aspectJ的-->
        <!--SpringAOP开发包-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <!--aop联盟-->
        <dependency>
            <groupId>aopalliance</groupId>
            <artifactId>aopalliance</artifactId>
            <version>1.0</version>
        </dependency>


        <!--AspectJ库-->
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
            <version>1.8.9</version>
        </dependency>
        <!--Spring整合aspectJ的包-->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aspects</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>

    </dependencies>
```

2. 创建配置文件，引入aop命名空间。用IOC容器管理相关对象
```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop" xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd">
    <!--开启aspectJ的注解开发，自动代理-->
    <aop:aspectj-autoproxy/>
     <!--目标类=====-->
    <bean id="productDao" class="com.imist.aspectJ.demo1.ProductDao"/>
    
    <!--定义切面,这里可以不用id，因为没有在其他地方使用-->
    <bean class="com.imist.aspectJ.demo1.MyAspectJAnno"/>
</beans>
```

3. 为目标类，定义切面类（用`@Aspect`注解），增强指定方法

```java
@Aspect
public class MyAspectJAnno {

    @Before(value = "execution(* com.imist.aspectJ.demo1.ProductDao.*(..))")
    public void before(){
        System.out.println("前置通知....");
    }
}

```
#### 前置通知`@Before `可以在方法中传入JoinPoint对象，用来获得切点信息
```
    
     // 对指定包下指定类的所有方法进行前置增强
    @Before(value = "execution(* com.imist.aspectJ.demo1.ProductDao.*(..))")
    public void before(){
        System.out.println("前置通知....");
    }
    //仅仅增强指定方法
    @Before(value = "execution(* com.imist.aspectJ.demo1.ProductDao.save(..))")
    public void beforeSave(JoinPoint joinPoint){
        System.out.println("前置通知...."+joinPoint);
    }
```

#### `@AfterReturning`后置通知
* 通过returning属性，可以定义方法的返回值作为参数传递给后置通知的形参
```
    @AfterReturning(value = "execution(* com.imist.aspectJ.demo1.ProductDao.update(..))" ,returning = "result")
    public void afterReturning(Object result){
        System.out.println("后置通知================"+result);
    }
```

#### `@Around` 环绕通知
* `around`方法的返回值就是目标代理方法执行返回值；
* 参数 `ProceedingJoinPoint` 可以拦截目标方法的执行
```
    @Around(value = "execution(* com.imist.aspectJ.demo1.ProductDao.delete(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕前通知=========");
        //这一句不执行那么目标方法就会被拦截，不会执行
        Object obj = proceedingJoinPoint.proceed();
        System.out.println("环绕后通知=========");
        return obj;
    }
```

#### `@AfterThrowing`异常抛出通知
* 通过设置 `throwing`属性，可以设置发生异常对象的参数，然后得到异常信息
```
    @AfterThrowing(value = "execution(* com.imist.aspectJ.demo1.ProductDao.findOne(..))",throwing = "exception")
    public void afterThrowing(Throwable exception){
        System.out.println("异常抛出通知========="+ exception.getMessage());
    }
```

#### `@After` 最终通知
* 无论目标方法是否出现异常，最终通知总是会被执行的

#### 引介通知（略）


### 通过 `@Pointcut`为切点命名
* 在每个通知内定义切点，会造成工作量大，不易维护，对于重复的切点可以使用 `@Pointcut`进行定义
* 切点方法： private void 无参数的空方法，方法名为切点名；
* 当通知多个切点时可以使用 `||` 进行连接

```
    //使用创建的切点，方法名为切点名，多个切点之间用 || 连接
    @Before(value = "pointcut1()")
    public void beforeSave(JoinPoint joinPoint){
        System.out.println("前置通知================"+joinPoint);
    }
    /**
     * private  void + 无参数的空方法 来定义切点，方法名为切点名
     * 而不是直接才通知上定义切点，方便维护,并且可重用
     */
    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.save(..))")
    private void pointcut1(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.update(..))")
    private void pointcut2(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.delete(..))")
    private void pointcut3(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.findAll(..))")
    private void pointcut4(){}

    @Pointcut(value = "execution(* com.imist.aspectJ.demo1.ProductDao.findOne(..))")
    private void pointcut5(){}
```

### 基于AspectJ的XML方式开发aop

* 无论是注解方式还是xml的方式导包都是一致的
* 配置方式需要将目标类和切面类加入ioc容器，然后进行aop相关配置，如下代码:
```xml
<!--XML配置方式完成AOP开发-->
    <!--配置目标类-->
    <bean id="customerDao" class="com.imist.aspectJ.demo2.CustomerDaoImpl"/>

    <!--配置切面类，不同于注解方式，这里需要id -->
    <bean id="myAspectXML" class="com.imist.aspectJ.demo2.MyAspectXML"></bean>

    <!--aop的相关配置-->
    <aop:config>
        <!--配置切入点，匹配哪些类的那些方法应用增强-->
        <aop:pointcut id="pointcut1" expression="execution(* com.imist.aspectJ.demo2.CustomerDao.save(..))"/>
        <aop:pointcut id="pointcut2" expression="execution(* com.imist.aspectJ.demo2.CustomerDao.delete(..))"/>
        <aop:pointcut id="pointcut3" expression="execution(* com.imist.aspectJ.demo2.CustomerDao.update(..))"/>
        <aop:pointcut id="pointcut4" expression="execution(* com.imist.aspectJ.demo2.CustomerDao.findOne(..))"/>
        <aop:pointcut id="pointcut5" expression="execution(* com.imist.aspectJ.demo2.CustomerDao.findAll(..))"/>

        <!--配置aop切面，用于那种增强(通知)-->
        <aop:aspect ref="myAspectXML">
            <!--配置前置通知-->
            <aop:before method="before" pointcut-ref="pointcut1"/>
            <!--配置后置通知-->
            <aop:after-returning method="afterReturning" pointcut-ref="pointcut2" returning="result"/>
            <!--环绕通知-->
            <aop:around method="around" pointcut-ref="pointcut3"/>
            <!--异常抛出通知-->
            <aop:after-throwing method="afterThrowing" pointcut-ref="pointcut4" throwing="exception"/>
            <!--最终通知-->
            <aop:after method="after" pointcut-ref="pointcut5"/>
        </aop:aspect>

    </aop:config>
```

