<%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/11
  Time: 下午2:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"
isErrorPage="true"
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--
1.只能在错误页面中使用
2.有一个页面出现异常，指定一个错误处理的页面，交由错误页面进行处理

--%>
<%=exception.getMessage()%>
</body>
</html>
