<%@ page language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Hello World!</title>
</head>
<body>
<% //pageContext请求转发也可以根据  request.getRequestDispatcher()跳转
//    1.forward 完成请求转发
//    2.inclode 方法完成jsp界面的包含关系 动态指令
    //3. pageContext 可以来获取其他的内置对象
    //实际使用过程中 不可以同时存在
    //参数是路径，在当前页面同级目录
    //pageContext.include("header.jsp");
    pageContext.forward("a.jsp?name=imist");
%>
</body>
</html>