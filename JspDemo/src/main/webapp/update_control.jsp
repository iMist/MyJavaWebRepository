<%@ page import="com.imist.db.DBUtil" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.imist.bean.Emp" %><%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/11
  Time: 下午3:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--    这里的值是从上一个页面submit中提交而来的；就是value属性里面对应的值一次提交产生一次请求，请求key为 name请求参数为表单里面的value--%>
<%--    <input type="text" name="email" value="<%= request.getParameter("email")%>">--%>


   <%
       Map<String ,Emp> map = DBUtil.map;
       Emp emp =  map.get(request.getParameter("account"));
       emp.setName(request.getParameter("name"));
       emp.setEmail(request.getParameter("email"));
   %>

<h3 align="center"> 修改员工信息成功</h3>
</body>
</html>
