<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
config对象：<%= config.getServletName()%>
<br/>
config对象 获取初始化参数：<%= config.getInitParameter("age")%>

<% pageContext.setAttribute("age",12);%>
<br>
pageContext作用域中获取的值<%= pageContext.getAttribute("age")%>
<br>
page 的基本信息 <%= this.getServletInfo() %>
<%
    request.setAttribute("name","Imook");
    //这里的跳转传入了request和response对象
    //request.getRequestDispatcher("result.jsp").forward(request,response);
%>
<h3 align="center" >人事管理系统登录页面</h3>
<hr>
<!-- action代表了服务器端的处理程序 -->
<form action="control.jsp">
    <table align="center">
        <tr>
            <td>
                账号：
            </td>
            <td>
                <input type="text" name="account"/>
            </td>
        </tr>
        <tr>
            <td>
                密码：
            </td>
            <td>
                <input type="password" name="password"/>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="登录"/>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
