<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"
         errorPage="error.jsp"
         import="com.imist.db.* ,com.imist.bean.*"
%>
<%@ page import="java.util.Map" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--获取用户名以及密码，并且需要去调用DBUtil当中的方法来判断是否存在指定的信息
    1.如果正确返回成功页面
    2.如果错误，显示错误信息
    request获取请求信息
    getParameter(String name) 可以通过一个控件的name属性获取值

    out 对象是一个输出流对象，输出指定的信息 println（）

--%>

<h3 >响应的字符编码集 :<%=  response.getCharacterEncoding() %></h3>
<%
    String account = request.getParameter("account");
    String password = request.getParameter("password");
    //out.println("账号 :"+account + "密码:" +password);
    Emp emp = new Emp(account, null, password, null);
    boolean flag = DBUtil.selectEmpByAccountAndPassword(emp);
    Map<String ,Emp> map = DBUtil.map;
    if (flag) {
        Object o = application.getAttribute("count");
        if (o == null){
            application.setAttribute("count",1);
        }else {
            int count = Integer.parseInt(o.toString());
            application.setAttribute("count" ,count+1);
        }
        session.setAttribute("account",account);
        //设置session的有效会话时间
        session.setMaxInactiveInterval(5*60);

        //session.invalidate(); 销毁会话

%>
<h3 align="right">访问量 ： <%=  application.getAttribute("count")%></h3>
<h3 align="right"> <%= session.getAttribute("account")%></h3>
<h3 align="center"> 欢迎来到人事管理系统首页！</h3>
<hr>
<table align="center" border = "1" width="500px">
    <tr>
        <td>
            账号
        </td>
        <td>
            员工姓名
        </td>
        <td>
            邮箱
        </td>
        <td>
            修改
        </td>
    </tr>
    <%
        for (String key : map.keySet()){
            Emp e = map.get(key);
            %>
    <tr>
        <td>
            <%= e.getAccount()%>
        </td>
        <td>
            <%= e.getName()%>
        </td>
        <td>
            <%= e.getEmail()%>
        </td>
        <td>
            <%--相邻两个jsp页面传递数据的时候，通过url参数的方式来传递数据--%>
            <%--规则： 1. 资源？key=value& key = value--%>
            <a href="update.jsp?account=<%=e.getAccount()%>&name=<%=e.getName()%>&email=<%= e.getEmail()%>">修改</a>
        </td>
    </tr>
    <%
        }
    %>
</table>
<%
    } else {
        //out.println("账号密码错误！");
        throw new Exception("账号和密码错误");
    }
%>
</body>
</html>
