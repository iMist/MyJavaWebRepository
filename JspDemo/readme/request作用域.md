    ### 两个相邻的web资源之间共享同一个请求对象的时候使用

    第一个界面
    
    
    request.setAttribute("name","Imook");
    //这里的跳转传入了request和response对象 
    request.getRequestDispatcher("result.jsp").forward(request,response);
 
    第二个界面
    
    从request作用域中获取的数据<%= request.getAttribute("name")%>
    