### exception 对象

 1 可能出现异常的界面抛出异常,然后声明错误页面
 
 >else {
          //out.println("账号密码错误！");
          throw new Exception("账号和密码错误");
      }
 ---------------------------
 ```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"
           errorPage="error.jsp"
           import="com.imist.db.* ,com.imist.bean.*"
  %>
```
 
 2 声明错误页面
 
  ```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"
    isErrorPage="true" 
``` 
 
 3.使用异常对象
 
```jsp
  <%=exception.getMessage()%>
```
 
 