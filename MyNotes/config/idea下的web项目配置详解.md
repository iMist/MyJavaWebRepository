### Idea  Web项目配置详解

1. 创建一个新的web工程 ，这里选择maven当然也可以选择javaEE
   
   ![](images/image1.png)

2. 创建项目 设置GroupId,ArtifactId以及项目版本  

    ![](images/image2.png)
3. 和源码目录资源目录同级的路径下创建一个新的目录，目录结构如下  
   
     ![](images/image3.png)
 
4. 打开PrijectStructure 配置Facets ,添加Web项，
    
     ![](images/image4.png)
     
5. 去掉原有Desploy路径,如果正确的话不去掉也ok；当然这里采取的是习惯性的目录结构，默认的也是可以的
    
     ![](images/image5.png)
     
6. 这里指定对应的web.xml路径
    ![](images/image6.png)   
     
7. 配置web资源的项目路径和工程名 http://localhost:8080/+ 工程名，不指定的话，直接就可以访问 ，默认是不指定的，idea
    提供了一套自己的访问tomcat的方式
    
     ![](images/image7.png)    
     
8 .接下来添加Artifact   
    ![](images/image8.png)    
     
9.选择打包成war包

 ![](images/image9.png)      
 
10 . 添加Tomcat
    
  ![](images/image10.png)   

11. tomcat的浏览器的访问路径配置
  ![](images/image13.png) 
  
12.这里注意下tomcat的访问路径是部署的路径,http://localhost:8080/+ 工程名/来访问
    所以tomcat下的 ApplicationCoontext 就不用配置了（直接就匹配部署的路径）不然访问的真实路径需要http://localhost:8080/+ 工程名/+ApplicationCoontext/
    才能访问

 ![](images/image11.png)       
 
  ![](images/image12.png)  
   
   
     
       