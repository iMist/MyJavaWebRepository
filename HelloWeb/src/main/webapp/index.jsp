<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 11:21
  To change this template use File | Settings | File Templates.
--%>
<%--展示的字符集和页面保存的字符集--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>JSP内容输出表达式</title>
</head>
<body>
<%--语法格式--%>
<%--在输出完整页面信息的时候 很明显表达式输出由于输出语句--%>
<%! int i = 0 ;%>
i的值是 :<%= i %>
<br>
i的值是：<% out.println(i);%>

</body>
</html>
