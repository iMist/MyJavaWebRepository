<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>//注释语法  <%-- --%></title>
</head>
<body>

<%!
String str = "hello world";
%>
<% //在保证程序基本运行的逻辑的情况下 ，既可以使用脚本注释也可以使用Java注释 ，
    //当既有Java也有页面、 脚本的情况下 用脚本注释全部
    out.println(str);
%>
</body>
</html>
