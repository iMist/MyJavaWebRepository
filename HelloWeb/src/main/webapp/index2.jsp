
<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="utf-8" %>
<%--注意界面显示编码 ，文件的编码统一一下--%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>Hello World!</h2>
<%--Jsp声明语法
定义成员变量以及成员方法
不能直接包含程序的输出语句--%>
<%! String str = "hello world ";
    String getStr(){
        return "Hello Web";
    }
%>
<hr>
<%  out.println(this.str);%>
<hr>
<%= this.getStr()%>

</body>
</html>
