<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--声明式成员变量 ，每次访问都会刷新--%>
<%! int totalCount = 0 ;%>
<%--局部变量--%>
<% int localCount = 0 ;
    totalCount ++;
    localCount ++;
%>
<%--out 就是输出到页面的--%>
<%
out.println("totalCount : "+totalCount);
out.println("<br>");
out.println("localCount:"+localCount);
%>
</body>
</html>
