<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 11:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%-- 自由编程 -- 局部的其实就是一个方法--%>
<%
    int sum = 0;
    for (int i = 0; i <= 100; i++) {
        sum = sum + i;
    }
    out.println(sum);
%>
</body>
</html>
