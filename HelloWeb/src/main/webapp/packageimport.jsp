<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/10
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page import="java.io.*" %>--%>
<%--<%@ page import="java.util.Date" %>--%>
<%@ page import="java.util.Date ,java.text.*" %>
<html>
<head>
    <title>包导入语法</title>
</head>
<body>
和Java一样支持通配符，同时也支持多包导入，逗号分隔 ，作为一个整体字符串
<br>
<%!  public String getDate(){
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss");
    Date date = new Date();
    return  format.format(date);
}
%>
<%= getDate()%>
</body>
</html>
