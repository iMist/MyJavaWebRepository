package com.imist.filter;


import javax.servlet.*;
import java.io.IOException;


public class CharacterEncodingFilter implements Filter {
    private FilterConfig config;



    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("characterEncoding Filter init ...");
        this.config = filterConfig;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("characterEncoding Filter doFilter ...");
        String charset = config.getInitParameter("charset");
        request.setCharacterEncoding(charset);
        //通知web服务器已经请求拦截处理完成
        chain.doFilter(request,response);
    }



    public void destroy() {
        System.out.println("characterEncoding Filter distory ...");
    }

    public FilterConfig getConfig() {
        return config;
    }

    public void setConfig(FilterConfig config) {
        this.config = config;
    }

}
