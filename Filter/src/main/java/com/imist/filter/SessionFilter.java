package com.imist.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String loginUser = (String) httpServletRequest.getSession().getAttribute("loginUser");
        if (loginUser == null){
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath()+"/index.jsp?flag=1");
            return;
        }else {
            chain.doFilter(request,response);
        }
    }

    public void destroy() {

    }
}
