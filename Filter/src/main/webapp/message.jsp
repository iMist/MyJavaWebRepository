<%@ page import="com.imist.Message" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/13
  Time: 下午9:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<% String user = (String) session.getAttribute("LoginUser");
    String subFlag = request.getParameter("subFlag");
    List<Message> messages = (List<Message>) session.getAttribute("messages");

%>
<html>
<head>
    <title>留言板</title>
    <link href="form.css" rel="stylesheet" type="text/css"/>
    <link href="table.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        var subflag =
        <%= subFlag%>
        if ("1" == subflag) {
            //alert("留言成功")
        }
    </script>
</head>
<body>
<form action="messageSub.jsp" method="post" class="smart-green">
    <h1> 留言板</h1>
    <label>
        <span>留言人</span>
        <input id="user" type="text" name="user" value="<%= user%>" readonly>
    </label>
    <label>
        <span> 标题</span>
        <textarea id="title" name="title"> </textarea>
    </label>
    <label>
        <span> 内容</span>
        <textarea id="content" name="content"> </textarea>
    </label>
    <label>
        <input type="submit" class="button" value="提交"/>
    </label>
</form>
<br>
<table id="table-3" width="85%" align="center">
    <tr>
        <th width="15%">留言人></th>
        <th width="15%">标题</th>
        <th width="75%">内容</th>
    </tr>
    <%
        if (messages != null) {
            for (Message message : messages) {
    %>

    <tr>
        <td><%= user %>
        </td>
        <td><%= message.getTittle() %>
        </td>
        <td><%= message.getContent() %>
        </td>
    </tr>
    <%
            }
        }
    %>
</table>
</body>
</html>
