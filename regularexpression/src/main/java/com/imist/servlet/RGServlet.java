package com.imist.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "com.imist.servlet.RGServlet")
public class RGServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String usernamePattern = "[a-zA-Z]{6,12}";
        String passwordPattern = "[0-9a-zA-Z]{6,12}";
        boolean ismatch = username.matches(usernamePattern);
        ismatch = password.matches(passwordPattern);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
