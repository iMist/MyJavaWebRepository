### 主要技能点
* 正则表达式的前端校验方式
* 正则表达式的后端校验

### 正则表达式
* 前端校验的优点 
   > 能够对数据进行初步的筛选，减少后台服务器的压力，使用html5校验简单易用 pattern
   
* 弊端 : 通过一些手段可以轻易绕过前端校验   
   
* 前端校验 示例代码 
```html
    <form align="center" action="/com.imist.servlet.RGServlet" method="post">
    用户名:<input type="text" name="username" pattern="[a-zA-Z]{6,12}" required="required" placeholder="请输入6-12位的字母"></p>
    密&nbsp;码:<input type="password" name="password" pattern="[0-9]{6,}" required="required" placeholder="请输入至少6位的数字"></p>
    手机号:<input type="text" name="phone" pattern="1[3578]\d{9}" required="required" placeholder="请输入手机号"></p>
    邮&nbsp;箱:<input type="email" name="email" required="required" placeholder="邮箱"></p>
    <input type="submit" value="提交"> <input type="reset" value="重置">
</form>
```  
>注意: 1. pattern = "" 设置匹配规则
       2. required =  required 表示按照自定义或者系统声明的类型进行匹配并且当用户输入不匹配的时候提示
       3. 当声明类型为邮箱的时候 就不需要设置匹配规则了，这个是自带的匹配
       

### 正则表达式的语法规则
 > []标识区间占一个字符 ,{}匹配字符的位数
 
* [abc]  匹配a,b或者c 匹配一个字符
* [a-zA-Z] a-z 或者A-Z  包括两头的字母（闭区间）
* \d      数字: 等同于[0-9]
* X{n}    X恰好n次  
* X{n,}   X至少n次
* X{n，m}  X至少n次但是不超过m次



* \D   非数字 
* \s   空白字符(如空格 \t制表范围 ,\n换行)
* \S   非空白字符(除了空白字符以外其他都可以)
* \w   单词字符:[a-zA-Z_0-9]
* \W   非单词字符

> 在正则表达式中“ ^ ”表示正则的起始标记 “$” 表示结束标记(可以不写);




