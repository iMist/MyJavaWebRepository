<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/13
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>通过jstl+el 表达式forEach 集合</title>
</head>
<body>
<%--<c:out value="${list}"></c:out>--%>
<%--items = "${集合Key}" value = "遍历的每一条item ,就是for循环中的局部变量"--%>

<c:forEach items="${list}" var="Map">
    ${Map.shopName} : ${Map.address} : ${Map.price}<br>
</c:forEach>

<table  align="center" border="1px" cellspacing="0px">
    <tr>
        <td>商品名称</td>
        <td>商品地址</td>
        <td>价格</td>
    </tr>
    <c:forEach items="${list}" var="Map" >
       <tr>
           <td>${Map.shopName}</td>
           <td>${Map.address}</td>
           <td>${Map.price}</td>
       </tr>
    </c:forEach>
</table>
</body>
</html>
