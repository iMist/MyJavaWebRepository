<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/13
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>jstl中的if choose标签</title>
</head>
<body>

<c:set var="age" value="12" scope="request"></c:set>

<%--if标签 test接判断条件 ${条件表达式}}，如果条件为true则执行标签中的内容 --%>
<c:if test="${age==12}">
    年龄12
</c:if>
<br>
hello world
<br>
<%--choose 标签相当于if else 不可以单独使用 ,要和when(if)以及otherwise(else)一起使用
引号必须包裹表达式（不可以带有空格），但是表达式内部只要语法正确即可
--%>

<c:choose>
    <c:when test="${age == 12 }">
        年龄十二岁
    </c:when>
    <c:otherwise>
        年龄不是十二岁
    </c:otherwise>
</c:choose>
</body>
</html>
