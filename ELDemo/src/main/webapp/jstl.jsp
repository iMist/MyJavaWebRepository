<%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/12
  Time: 下午10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>jstl 中的 set out remove 标签</title>
</head>
<body>
    <%--set out remove   var 和value 相当于 键值对的关系 scope 就是数据操作的作用域 标签--%>
    <%--set标签主要是往后指定的域中存放数据--%>
    <c:set var="username" value="张三" scope="request" ></c:set>
    <%--将数据打印显示--%>
    <c:out value="${username}"></c:out>
    <hr>
    <%--移除request域中key为username的数据 --%>
    <c:remove var="username" scope="request"></c:remove>
    <c:out value="${username}"></c:out>
    <hr>
</body>
</html>
