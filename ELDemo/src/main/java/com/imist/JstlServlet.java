package com.imist;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JstlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //创建一个list集合，向list集合中存放多条数据
        Map<String ,Object> dataMap = new HashMap<String, Object>();
        dataMap.put("shopName","联想笔记本");
        dataMap.put("address","beijing");
        dataMap.put("price","4999.99");
        Map<String ,Object> dataMap2 = new HashMap<String, Object>();
        dataMap2.put("shopName","华为笔记本");
        dataMap2.put("address","深圳");
        dataMap2.put("price","5999");
        Map<String ,Object> dataMap3 = new HashMap<String, Object>();
        dataMap3.put("shopName","苹果笔记本");
        dataMap3.put("address","纽约");
        dataMap3.put("price","20000");
        List<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
        list.add(dataMap);
        list.add(dataMap2);
        list.add(dataMap3);
        //2.将其保存到request域中
        request.setAttribute("list",list);
        //3.然后在页面取出request域中的集合
        request.getRequestDispatcher("/jstl3.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
