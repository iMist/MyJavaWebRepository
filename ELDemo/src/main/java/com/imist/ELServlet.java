package com.imist;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ELServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.首先获取username和age属性值
        String username = request.getParameter("username");
        String age = request.getParameter("age");
        //2.将获取到的数据保存到request 域中
        request.setAttribute("username",username);
        request.setAttribute("age",age);
        //3.跳转到showEL.jsp页面；
        request.getRequestDispatcher("showEL.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
