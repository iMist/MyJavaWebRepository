### EL 表达式 

>通过 ${username}达到获取对应的数据 可以直接在jsp页面中使用不需要 使用<% %>


```

 ${username}
 相当于 
 <% page/request/session/applicaton.getAttribute("username")%>

```

>以上四种优先级依次递减的 

### 取出指定作用域下的数据

* page 对应 pageScope

* request 对应 requestScope

* session 对应 sessionScope

* application 对应 applicationScope

```
    ${pageScope.username}
    ${requestScope.username}
    ${sessionScope.username}
    ${applicationScope.username}
```

### .和[]的使用 ，获取自定义属性的值

```
     //其中user是自定义属性的key
     $[user.username]
     $[user.password]
     等价于
     $[requestScope.user.username]
     $[requestScope.user.password]
     等价于
     ${user["username"]}
     ${user["password"]} 
     
     //第三种情况适用于 属性key中有特殊符号的情况 如${header[Accept-Language]}

```