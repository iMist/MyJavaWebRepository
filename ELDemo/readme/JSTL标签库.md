### JSTL标准标签库 （javaServerPager Standard Tag Library） 

* 关于jstl标签库  jstl1.2版本之后是不需要standard.jar的包的
![如图所示:](images/image1.png)
* 通常会与El表达式合作实现jsp页面的编码

### 为什么要使用jstl 标签

1. 在jsp中不建议直接书写Java代码，引入之后直接使用不需要<%%> 包裹
    避免使用Java代码使程序利于扩展

2. EL表达式 虽然可以解决在jsp中不用书写Java的问题，但是对于复杂的数据（数组集合等）
    取值依旧比较麻烦
    
3. 使用Jstl标签库 配合EL表达式能够很好的解决取复杂类型数据的问题简化书写


## 具体使用 
1. 导包 
 ```
    <properties>
         <servlet-api-version>3.0-alpha-1</servlet-api-version>
         <jstl-version>1.2</jstl-version>
         <taglibs-version>1.1.2</taglibs-version>
     </properties>
     
     
       <dependency>
            <groupId>jstl</groupId>
            <artifactId>jstl</artifactId>
            <version>${jstl-version}</version>
        </dependency>
        <dependency>
            <groupId>taglibs</groupId>
            <artifactId>standard</artifactId>
            <version>${taglibs-version}</version>
        </dependency>
```
2 . 引入核心标签库

```
    <%@taglib uri="http://java.sun.com/jsp/jstl/core"    prefix="c" %>
```

### 标签库语法  

>注意:这里的 var带有类型推断的性质，虽然value 用引号包裹，但是不一定是字符串
 另外获取指定域的数据参考 EL表达式的写法

* out set 和remove 标签

```jsp
<%--set out remove   var 和value 相当于 键值对的关系 scope 就是数据操作的作用域 标签--%>
    <%--set标签主要是往后指定的域中存放数据--%>
    <c:set var="username" value="张三" scope="request" ></c:set>
    <%--将数据打印显示 ，这里取指定域的数据和EL表达式差不多requestScope.username--%>
    <c:out value="${username}"></c:out>
    <hr>
    <%--移除request域中key为username的数据 --%>
    <c:remove var="username" scope="request"></c:remove>
    <c:out value="${username}"></c:out>
    <hr>
```         

#### if choose语法  

* if标签 test接判断条件 ${条件表达式}}，如果条件为true则执行标签中的内容      

```jsp
<c:if test="${age==12}">
    年龄12
</c:if>
``` 
>错误示范 

![注意符号问题，符号戏不可以太多,引号牢牢抱住${}表达式](images/image2.png)

* choose 标签 

><%--choose 标签相当于if else 不可以单独使用 ,要和when(if)以及otherwise(else)一起使用--%>


```jsp
<c:choose>
    <c:when test="${age == 12}">
        年龄十二岁
    </c:when>
    <c:otherwise>
        年龄不是十二岁
    </c:otherwise>
</c:choose>
```     

* 迭代标签 forEach ,可以操作数组和集合
> 语法: items = "${集合Key}" value = "遍历的每一条item ,其实就是for循环中的局部变量"

```jsp
<c:forEach items="${list}" var="Map" >
    ${Map.shopName} : ${Map.address} : ${Map.price}<br>
</c:forEach>
```