### jar包导入异常导致500 错误


* 问题描述 ,Maven导入包编译正常，但是部署上tomcat出现如下错误 ,手动在WEB-INF/lib下导入jar包也编译运行均正常

```
org.apache.catalina.core.StandardWrapperValve.invoke Servlet.service() for servlet [jsp] in context with path [] threw exception 
[The absolute uri: http://java.sun.com/jsp/jstl/core cannot be resolved in either web.xml or the jar files deployed with this application] with root cause org.apache.jasper.JasperException: The absolute uri: http://java.sun.com/jsp/jstl/core cannot be resolved in either web.xml or the jar files deployed with this application
```
* 初步原因,在tomcat的lib下没有对应的jar文件


>解决办法: 将对应的jar文件拖入tomcat下的lib中即可


* 考虑用Maven自动解决(待完善)