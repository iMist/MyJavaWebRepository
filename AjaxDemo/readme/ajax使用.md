## Ajsx 使用

### ajax概述

>Ajax 是一种快速创建动态网页的技术 ，能够实现动态刷新 ;

### AJax 好处
 > 更新时只更新局部用户体验更好，由于只刷新局部的数据，对后台的压力小
  
### 引入jquery
1. 在webapp目录下创建js目录
2. 将jquery对应的文件添加进目录
   

   ![](images/image1.png)

 >引入在线的jQuery
    ```
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">
    ```
 
3. 在页面的head标签中引入

    ![](images/image2.png)

4. 在scripe标签中使用jquery 表达式

    ![](images/image3.png)

* 示例代码 注意参数不要写错了 
 
 >  `url` :代表 请求的路径
    `type` : 代表提交的方式 
    `data` : 用来传递的数据
    `dataType`: "ajax接收后台数据的类型"
    `async`: true,请求是否异步，默认为异步，这也是ajax重要特性  
    `success` : 请求成功的回调方法

>注意：当使用ajax和后台交互的时候，后台是不能跳转到其他页面的    
```

<script type="text/javascript"> 
    $("#login").click(function() {
       $.ajax({
           url :"/LoginServlet",
           type: "post",
           dataType:"json",
           // 这里的数据对应servlet接收的数据  接受的数据key:通过选择器获取的数据
           data:{
               username : $("input[name = username]").val(),
               password : $("input[name = password]").val()
           },
           success: function (result) {
                if (result.flag){
                    //如果登陆成功则跳转到成功登陆页面
                    window.location.href("/success.jsp");
                }else {
                    //跳转回index.jsp页面同时在登陆页面给用户一个友好的提示
                    $(".tip").text("账号或者密码错误");
                }
           }
       });
    });
</script>
```

### 用js实现ajax 请求

* 代码示例

```javascript

    function showData( flag) {
        //创建一个XMLHttpRequest对象
        var xmlHttp = new XMLHttpRequest();
        //2.规定请求的方式，url 以及是否异步请求
        xmlHttp.open("GET","/AjaxServlet?flag=" + flag, true);
        //3.发送请求
        xmlHttp.send();
        //4.xmlHttp.readyState 请求已完成响应已就绪，status 代表响应一切正常
        xmlHttp.onreadystatechange = function (ev) {
            if (xmlHttp.readyState = 4 && xmlHttp.status == 200){
                document.getElementById("div1").innerHTML = xmlHttp.responseText ;
            }
        }
    }

```


### Ajax 跨域 





    