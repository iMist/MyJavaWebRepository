package com.imist;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AjaxServlet")
public class AjaxServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1. 获取ajax传递过来的信息
        String flag = request.getParameter("flag");
        String data = "";
        //2.需要返回的数据信息
        if ("1".equals(flag)){
            data = "javaWeb";
        }else if ("2".equals(flag)){
            data = "Android";
        }
        //3.将数据返回给ajax
        response.getOutputStream().write(data.getBytes("utf-8"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
