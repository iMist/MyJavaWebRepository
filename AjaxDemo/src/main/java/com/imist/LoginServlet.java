package com.imist;

import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        JSONObject jsonObject = null;
        if ("admin".equals(username) && "123456".equals(password)){
            System.out.println("username :" +username + "password:"+password);
            jsonObject = new JSONObject("{flag:true}");
        }else {
            //如果登陆失败，则给ajax返回一个数据
            jsonObject = new JSONObject("{flag:false}");
            //request.getRequestDispatcher("/index.jsp").forward(request,response);
        }
        //将数据回写到ajax;
        String jsonStr = jsonObject.toString();
        //响应客户端的请求
        response.getOutputStream().write(jsonStr.getBytes("utf-8"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
