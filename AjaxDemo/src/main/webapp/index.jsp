<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/13
  Time: 12:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String bashPath = request.getScheme() +"://" + request.getServerName()+":" +request.getServerPort() + path +"/";
%>
<html>
<head>
    <title>Login</title>
    <style type="text/css">
        input{
            width:250px;
            height:25px;
        }
        #login{
            width:255px;
            height:35px;
            background-color:#FF2611;
            border:0px;
            cursor:pointer;
            color:white
        }
        .c1{
            font-size:24px;
            color:black;
            font-weight:bolder
        }
        .c2{
            font-size:14px;
            color:#666;
        }
        .c3{
            font-size:14px;
            color:red;
        }
    </style>
    <script type="text/javascript" src="js/jquery-1.4.2.js"></script>
</head>
<body style="text-align:center;">
<%--<form action="/LoginServlet"  method="post">--%>
<table align="center">
    <tr>
        <td>
            <span class="c1">欢迎登录</span>&nbsp;
            <span class="c2">没有帐号？</span>
            <span class="c3">立即注册</span>
        </td>
    </tr>
    <tr>
        <td><input type="text" name="username" placeholder="请输入登录邮箱/手机号"><span class="tip" style="color:red;font-size:12px"></span></td>
    </tr>
    <tr>
        <td><input type="password" name="password" placeholder="6-16位密码，区分大小写，不能空格"></td>
    </tr>
    <tr>
        <td>
            <!-- <input type="submit" value="登录"  id="login"> -->
            <input type="button" value="登录"  id="login">
        </td>
    </tr>
</table>
<%--</form>--%>
</body>
<%--这里的:有点赋值的含义--%>
<script type="text/javascript">
    $("#login").click(function() {
       $.ajax({
           url :"/LoginServlet",
           type: "post",
           dataType:"json",
           // 这里的数据对应servlet接收的数据  接受的数据key:通过选择器获取的数据
           data:{
               username : $("input[name = username]").val(),
               password : $("input[name = password]").val()
           },
           success: function (result) {
                if (result.flag){
                    //如果登陆成功则跳转到成功登陆页面 ,熟悉 =和() 的使用情况
                    // window.location.href("/success.jsp");
                    window.location.href = "/success.jsp";
                }else {
                    //跳转回index.jsp页面同时在登陆页面给用户一个友好的提示
                    $(".tip").text("账号或者密码错误");
                }
           }
       });
    });
</script>
</html>
