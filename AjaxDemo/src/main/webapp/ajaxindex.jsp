<%--
  Created by IntelliJ IDEA.
  User: iMist
  Date: 2018/8/13
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Ajax javascript 实现</title>
</head>
<script type="text/javascript">
    function showData( flag) {
        //创建一个XMLHttpRequest对象
        var xmlHttp = new XMLHttpRequest();
        //2.规定请求的方式，url 以及是否异步请求
        xmlHttp.open("GET","/AjaxServlet?flag=" +flag, true);
        //3.发送请求
        xmlHttp.send();
        //4.xmlHttp.readyState 请求已完成响应已就绪，status 代表响应一切正常
        xmlHttp.onreadystatechange = function (ev) {
            if (xmlHttp.readyState = 4 && xmlHttp.status == 200){
                document.getElementById("div1").innerHTML = xmlHttp.responseText ;
            }
        }
    }
</script>
<body>
    <input type="button" value="查看1"  onclick="showData(1)">
    <br>
    <input type="button" value="查看2" onclick="showData(2)">
<div id="div1" style = "width:300px ;heigh:300px"></div>
</body>
</html>
