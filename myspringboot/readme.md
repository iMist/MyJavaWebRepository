### 特点
* 简单易用
* 可独立运行的Spring项目
* `习惯优于配置` ，极大地提高了开发效率
* 极简的组件依赖，自动发现与自动装配
* 提供运行时的应用监控
* 与分布式和云计算的天然集成

### 通过idea 提供的工具一键完成SpringBoot构建
1.  选择SpringIntializr,创建项目,一定要是java8以上的版本
![](images/image1.png)
2. 配置项目相关的名字，组名包名，打包方式等
![](images/image2.png)
3.选择项目需要的启动器
![](images/image3.png)

4. 创建完成等待一会，会自动引入项目依赖的jar，创建相关包和主启动类，以及SpringBoot约定的文件；


### SpringBoot的入口类

* 入口类命名通常以 `*Application` 结尾
* 入口类上增加 `@SpringBootApplication`注解
* 利用`SpringApplication.run()` 方法启动应用


### SpringBoot启动流程

![](images/image4.png)

### SpringBoot基本配置
![](images/imag5.png)
* `server.port=8001` 配置端口号
* `server.servlet.context-path=/myspringboot` 配置上下文，当部署的多个应用都有out这个映射地址的话，通过上下文进行区分
* 输出日志内容到文件 logging.file=/Users/imist/MyRepository/MyJavaWebRepository/myspringboot/images/myspringboot.log
* 设置日志文件的输出级别 `logging.level.root=error` debug>info>warn>error>fatal(灾难性日志如服务器宕机)
* 打开调试 ,查看程序底层的细节，自动生成报告 `debug=true`

* 数据源配置 ,当Springboot使用jpa或者jdbc连接数据库的时候会自动加载数据库配置信息进行数据库连接操作
```
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=jdbc:mysql://localhost:3306/db
spring.datasource.username=root
spring.datasource.password=123456
```

#### Mac出现端口占用的情况
* `The Tomcat connector configured to listen on port 80 failed to start. The port may already be in use or the connector may be misconfigured.`

    * `lsof -i :80 ` 查看端口占用的pid
    * `kill pid` 杀掉占用指定端口的程序；
    
> 在mac os中，非root用户是无法使用小于1024的常用端口的。如果开发中需要用到80端口, 就要设置端口转发。
这里暂不深究，知道即可    
    
    
 ###  配置文件管理优化

* 属性文件 `application.properties` 上线后一旦发现错误，不利于维护出错概率增大

* YAML格式 ：`application.yml`

#### `YAML` 的语法

* YAML是一简洁的非标记语言，YAML以数据为中心，使用空白，缩进，分行组织数据从而使得表示更加简洁易读
* YAML语法格式
    * 标准格式 : key:(空格)value
    * 使用空格代表层级关系 ，以 ":" 结束
    
    
#### SpringBoot 自定义配置项
* Springboot允许我们自定义配置项，在程序运行时允许动态加载，这为持续提供了良好的可维护性
* 在实际开发过程中，我们通常将项目的自定义信息放在配置文件中   

1. 在application.yam 中按照语法 进行自定义配置 
```yaml
mail:
 config:
  name: 商城
  description: 这是一个商城系统
  hot-sales: 20   # 首页显示多少热卖商品
  show-advert: true # 是否显示广告
```
2. 在属性上使用 `@Value` 注解为属性注入值，springboot 自动扫描`yml`文件将对应的key进行注入
    * 注意层级关系，注意调用语法
    * 注意值的属性是可以被转化的
    
 ```
       @Value("${mail.config.name}")
       private String name;
       @Value("${mail.config.description}")
       private String description;
       @Value("${mail.config.hot-sales}")
       private Integer  hotSales ;
       @Value("${mail.config.show-advert}")
       private Boolean  showAdvert;
```

#### 格式化输出的语句
* `String.format("name :%s,description: %s ,hot-sales : %s ,show-advert : %s" ,name,description,hotSales,showAdvert);`
  
  
### SpringBoot 环境配置文件

* SpringBoot可以针对不同的环境提供不同的配置文件
* Profile文件的默认命名格式为 `application-{env}.yml`
![](images/image6.png)
* 使用spring.profile.active来选定不同的profile
![](images/image7.png)

### 打包与运行

* 利用Maven的package命令生成独立可运行的jar包
* 利用 Java -jar XXX.jar启动Springboot应用
* jar包可自动加载同目录的application 配置文件

![](images/image8.png)

* 控制台输出 `[INFO] BUILD SUCCESS` 打包成功

* java -jar XXX.jar 实现部署

* 线上动态修改环境而不需要重新发布，只需要将对应的环境激活，然后重启服务即可
![](images/image9.png)

### 总结
* Springboot 用于快速构建Spring应用
* 采用`习惯优于配置`的设计理念
* SpringBoot的配置项与环境切换