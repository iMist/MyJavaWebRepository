### JSP 和servlet的区别
 > Jsp 偏向于页面展示
     Servlet 偏业务逻辑 ，jsp底层是servlet

### Servlet 路径 ：
   
> String basePath = request.getScheme()+":"+"//"+request.getServerName()+
    ":"+request.getServerPort()+"/"

### Servlet 生命周期

    初始化阶段 init() Servlet 第一次被访问的时候调用
    
    响应客户端请求阶段 service() 每次请求都会调用
    
    终止阶段 destory() 当Servlet被销毁的时候调用
    
    
### 主要方法

```
        response.getWriter() 获得PrintWriter 对象 用于向网页输出字符
        request.getContextPath(); 获取servlet访问路径下的工程名部分 
```    


### ServletContext  和ServletConfig 的区别

>     ServletContext ：javaWeb 工程用一个对象表示 就是ServletContext类型
>     ServletCongig 表示的就是某一个Servlet的配置文件 ，我们在web.xml文件中给某一个Servlet配置一些
>     配置信息，当服务器被启动的时候，这些配置信息就会被封装到ServletConfig对象中去
     
     


