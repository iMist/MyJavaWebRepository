<%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/11
  Time: 下午5:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%
    String basePath = request.getScheme()+":"+"//"+request.getServerName()+":"+request.getServerPort()+"/"
            //+request.getServletContext.getContextPath;
            //+request.getSession().getServletContext()+"/"
            ;
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
request.getContextPath() : <%= request.getContextPath()%>
<br>
request.getServletPath() : <%= request.getServletPath()%>
<br>
request.getServerName() : <%= request.getServerName()%>
<br>
request.getSession().getServletContext().getContextPath() : <%= request.getSession().getServletContext().getContextPath()%>
<br>
<div align="center">
    <form action="<%= basePath%>login" method="post">
        <p>用户名：<input type="text"  name="username"></p>
        <p>密&nbsp码：<input type="password"  name="password"></p>
        <p>
            <input type="submit" value="登录">
            <input type="reset" value="重置">
        </p>
    </form>
</div>
</body>
</html>
