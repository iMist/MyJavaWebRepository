<%--
  Created by IntelliJ IDEA.
  User: imist
  Date: 2018/8/11
  Time: 下午7:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>转发于重定向获取数据</title>
</head>
<body>

<%--<%
    String username = (String) request.getAttribute("username");
    out.println("username： " +username);
%>--%>

<%
    //String username = (String) application.getAttribute("username");
    //其实这里也是根据请求对象获取的ServletContext对象 而application是JSP的内置对象
    String username = (String) request.getServletContext().getAttribute("username");
    out.println(username);
%>

</body>
</html>
