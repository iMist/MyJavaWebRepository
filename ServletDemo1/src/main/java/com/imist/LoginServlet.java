package com.imist;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet");
        //获取表单提交过来的参数
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("username:"+username );
        System.out.println("password:"+password );
        //如果username=admin password = 123 则跳转到Success.jsp 否则跳转奥error.jsp；
        //这里由LoginServlet程序进行处理
        if (username.equalsIgnoreCase("admin") && password.equalsIgnoreCase("123")){
            //转发之进行一次请求 发由index.jsp提交给对应的Servlet进行处理
            request.getRequestDispatcher("/success.jsp").forward(request,response);
        }else{
            //重定向先请求对应的servlet程序，然后跳转到指定界面 index.jsp -> loginservlet -> fail.jsp
            //request.getRequestDispatcher("/fail.jsp").forward(request,response);
            response.sendRedirect("/fail.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost");
        doGet(req,resp);
    }
}
