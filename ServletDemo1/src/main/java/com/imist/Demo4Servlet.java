package com.imist;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 转发/重定向带数据给某一个页面
 */
public class Demo4Servlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //转发给某个jsp页面
        //request.setAttribute("username","来自转发 ： imist4");
        //request.getRequestDispatcher("/demo.jsp").forward(request,response);
        //通过重定向带数据过去

        System.out.println("重定向");
        //ServletContext context  = this.getServletContext();
        //ServletContext context = request.getServletContext();
        ServletContext context = request.getSession().getServletContext();
        context.setAttribute("username","imist4");
        response.sendRedirect("/demo.jsp");
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
