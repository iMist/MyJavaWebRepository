package com.imist;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet配置文件的内容会被封装到 ServletConfig配置文件中
 */
public class Demo3Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取当前Servlet的配置信息
        ServletConfig config = this.getServletConfig();
        Object result = config.getInitParameter("encode");
        System.out.println(this.getServletName()+"--->"+result);
    }
}
