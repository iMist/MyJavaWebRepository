package com.imist;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ServletContext 表示整个javaWeb工程
 *
 * Servlet 表示的是某一个 Servlet 的配置文件
 */
public class DemoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = this.getServletContext();
        //向context 域对象中存放数据
        context.setAttribute("username","iMist");
        Object result = context.getAttribute("username");
        System.out.println(this.getServletName()+"--->"+result.toString());
    }
}
