### SpringMvc 数据绑定
* 将http请求中的参数绑定到handler业务方法的形参列表当中；

* SpringMvc 数据绑定的实现流程
![](images/image1.png)

* 具体代码 
[](src/main/java/com/imist/controller/DataBindController.java)

#### 特别注意：
1. 注意配置默认的servlet，不然导致静态web资源被拦截；
2. 配置消息转换器注意命名空间；

```xml
<!--配置消息转换器，才能将请求参数转换成json,注意命名空间mvc下才有消息转换器 -->
    <mvc:annotation-driven>
        <mvc:message-converters>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter"></bean>
        </mvc:message-converters>
    </mvc:annotation-driven>
```

