package com.imist.entity;

import java.util.HashSet;
import java.util.Set;

public class CourseSet {

    private Set<Course> courses = new HashSet<Course>();

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    /**
     * 这一步非常重要否则无法完成绑定
     */
    public CourseSet() {
        courses.add(new Course());
        courses.add(new Course());
    }
}
