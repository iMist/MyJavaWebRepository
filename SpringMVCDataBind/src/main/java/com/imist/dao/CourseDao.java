package com.imist.dao;

import com.imist.entity.Course;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 这里需要将数据处理加入IOC容器,不然属性注入编译无法通过
 */
@Repository
public class CourseDao {
    private Map<Integer,Course> courseMap = new HashMap<Integer,Course>();

    public void add(Course course){
        courseMap.put(course.getId(),course);
    }
    public Collection<Course> getAll(){
        return courseMap.values();
    }
}
