package com.imist.controller;

import com.imist.dao.CourseDao;
import com.imist.entity.Course;
import com.imist.entity.CourseList;
import com.imist.entity.CourseMap;
import com.imist.entity.CourseSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DataBindController {

    @Autowired
    @Qualifier("courseDao")
    private CourseDao courseDao;

    //其数据由客户端传过来,由形参进行接收，完成数据转换和绑定
    /**
     * 基本数据类型
     * @param id
     * @return 返回到客户端的数据
     * @ResponseBody 将返回值作为响应体返回给客户端，可以是实体类
     * @RequestParam(value = "id") 对参数进行关联，对应request请求中的参数名
     * http://localhost:8080/baseType?id=1
     */
    @RequestMapping(value = "/baseType")
    @ResponseBody
    public String basicType(@RequestParam(value = "id") int id){
        return "id : "+id;
    }

    /**
     * 包装类型数据,可以防止数据值不存在或者类型错误的时候不至于报错；
     * @param id
     * @return
     * @PathVariable("id") 设置路径参数
     * http://localhost:8080/packageType?id=1
     */
    @RequestMapping(value = "/packageType/{id}")
    @ResponseBody
    public String packageType(@PathVariable("id") Integer id){
        return "id : "+id;
    }

    /**
     * 数组类型数据
     * @param name
     * @return
     * http://localhost:8080/arrayType?name=zhangsan&name=lisi
     *
     * 这里注意传参的名字必须按照形参进行传递，或者@RequestParam("") 注解对参数进行释义
     */
    @RequestMapping(value = "/arrayType")
    @ResponseBody
    public String arrayType(String[] name){
        StringBuffer buffer = new StringBuffer();
        for (String s : name) {
            buffer.append(s).append(" ");
        }
        return buffer.toString();
    }

    /**
     * 具体的业务模型 ，需要发送数据给页面，使用modelAndView不使用@ResponseBody
     * 将请求传过来的参数对应属性赋值给course对象
     * @param course
     * @return
     */
    @RequestMapping(value = "/pojoType")
    public ModelAndView pojoType(Course course){
        courseDao.add(course);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("courses",courseDao.getAll());
        return modelAndView;
    }


    /**
     * 这里注意参数不能直接使用List类型的参数，需要用一个实体类包裹，将list类型的参数作为实体类型的属性传入
     * @param courseList 包装了list类型的参数的实体类
     * @return
     */
    @RequestMapping("/listType")
    public ModelAndView listTyoe(CourseList courseList){
        for (Course course : courseList.getCourses()) {
            courseDao.add(course);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("courses",courseDao.getAll());
        return modelAndView;
    }

    /**
     * 和list类型一样，不可以直接传入，需要一个包装类
     * @param courseMap
     * @return
     */
    @RequestMapping(value = "/mapType")
    public ModelAndView mapType( CourseMap courseMap){
        //便利Map集合，先取出全部的Key，遍历key然后根据key取出集合的值
        for(String key : courseMap.getCourses().keySet()){
            Course course = courseMap.getCourses().get(key);
            courseDao.add(course);
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("courses",courseDao.getAll());
        return modelAndView;
    }


    @RequestMapping("/setType")
    public ModelAndView setType(CourseSet courseSet){
        for (Course course : courseSet.getCourses()) {
            courseDao.add(course);
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("courses",courseDao.getAll());
        return modelAndView;
    }


    /**
     * json支持：1.导入Jackson相关依赖，2.添加消息转换器的Bean
     * @RequestBody 将前端传过来的json格式的数据绑定到Course对象当中（由jackson框架进行数据转换完成绑定）
     * 注意默认的servlet配置，不然会拦截静态的web资源，导致js无效
     *
     * @RequestBody 将客户端传过来的json数据绑定到形参当中
     */
    @RequestMapping("/jsonType")
    @ResponseBody //直接将返回值作为响应体返回给客户端
    public Course jsonType(@RequestBody Course course){
        course.setPrice(course.getPrice() + 199);
        return course;
    }




}
