package com.imist.handler;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class MyHandler implements Controller {

    public ModelAndView handleRequest(javax.servlet.http.HttpServletRequest httpServletRequest, javax.servlet.http.HttpServletResponse httpServletResponse) throws Exception {
        //装载模型数据和逻辑视图
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据,自动添加到request作用域当中
        modelAndView.addObject("name","TOM");
        //添加逻辑视图
        modelAndView.setViewName("show");
        return modelAndView;
    }
}
