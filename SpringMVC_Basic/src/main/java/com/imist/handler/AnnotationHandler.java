package com.imist.handler;

import com.imist.entity.Goods;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * 用注解声明控制器bean
 */
@Controller
public class AnnotationHandler {

    /**
     * 业务方法，modelAndView完成数据传递和视图解析
     * @return
     */
    @RequestMapping("/modelAndViewTest")
    public ModelAndView modelAndViewTest(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("name","jerry");
        modelAndView.setViewName("show");
        return modelAndView;
    }

    /**
     *业务方法 ，model传值，String 进行视图解析，实际是将modelAndView进行拆分
     * @param model 模型数据
     * @return
     */
    @RequestMapping("/modelTest")
    public String modelTest(Model model){
        //设置模型数据
        model.addAttribute("name","iMist");
        //设置逻辑视图
        return "show";
    }

    @RequestMapping("/mapTest")
    public String mapTest(Map<String,String> map){
        //设置模型数据
        map.put("name","张三");
        //设置逻辑视图
        return "show";
    }

    /**
     * 添加商品并展示
     * @param goods 数据模型
     * @return
     */
    @RequestMapping("/addGoods")
    public ModelAndView addGoods(Goods goods){
        System.out.println(goods.getName() + "===" + goods.getPrice());
        ModelAndView modelAndView = new ModelAndView();
        //封装模型数据
        modelAndView.addObject("goods",goods);
        modelAndView.setViewName("show");
        //将模型数据放在request内置对象当中；
        return modelAndView;
    }

}
