
## SpringMvc详解

### SpringMVC核心组件
1. DispatcherServlet : 前置控制器
2. Handler 处理器 ：用于完成具体额业务逻辑；
3. HandlerMapping ： 将对应请求映射到对应的Handler;
4. HandlerInterceptor : 处理器拦截器  用于拦截处理请求；
5. HandlerExecutionChain : 处理器执行链 多个拦截器的执行链；
6. HandlerAdapter : 处理器适配器 ，表单数据的验证，转换，并且封装到javabean中；
7. ModelAndView : 装载了模型数据和视图信息；
8. ViewResolver : 视图解析器；将逻辑视图转换为物理视图，然后将结果渲染给客户端

### SpringMVC 实现流程
1. 客户端请求被DispatcherServlet接收。
2. DispatcherServlet将请求映射到Handler。
3. 生成Handler以及HandlerInterceptor。
4. 返回HandlerExecutionChain（Handler+HandlerInterceptor）。
5. DispatcherServlet通过HandlerAdapter执行Handler。
6. 返回一个ModelAndView。
7. DispatcherServlet通过ViewResolver进行解析。
8. 返回填充了模型数据的View，响应给客户端。

![](images/image1.png)


### 具体代码实现

1. 引入SpringMVC环境和Servlet,maven会自动引入相关jar包;
![](images/image4.png)
2. 实现具体的业务处理逻辑,继承Controller方法,重写 handleRequest方法
![](images/image6.png)
3. 创建springmvc.xml配置文件,完成具体的业务处理配置；
![](images/image5.png)
```xml
<!--1.配置handlerMapping 将url请求映射到handler-->
    <bean id="handlerMapping" class="org.springframework.web.servlet.handler.SimpleUrlHandlerMapping">
        <!--配置mapping-->
        <property name="mappings">
            <props>
                <!--配置test对应的请求handler-->
                <prop key="/test">testHandler</prop>
            </props>
        </property>
    </bean>
    <!--2. 配置具体的业务处理Handler-->
    <bean id="testHandler" class="com.imist.handler.MyHandler"> </bean>

    <!--3. 配置视图解析器-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <!--配置前缀-->
        <property name="prefix" value="/"></property>
        <!--配置后缀-->
        <property name="suffix" value=".jsp"></property>
    </bean>
```
4. 在`web.xml` 引入核心的DispatecherServlet配置,注意xml的配置路径
```xml
<servlet>
        <servlet-name>SpringMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--指定配置springmvc.xml配置文件的路径-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <!--类路径下的springmvc.xml-->
            <param-value>classpath:springmvc.xml</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>SpringMVC</servlet-name>
        <!--拦截所有请求-->
        <url-pattern>/</url-pattern>
    </servlet-mapping>
```

### 注解实现SpringMVC
1. 和xml一样需要引入环境，配置web.xml添加Dispatcher拦截处理所有的请求；
2. 在Spring mvc的xml配置文件中添加包扫描，以及视图解析器(参考xml）；
` <context:component-scan base-package="com.imist.handler"></context:component-scan>`
3. 创建具体的业务逻辑处理类, `@Controller` 声明逻辑处理Bean由IOC容器进行管理
4. 用 `@RequestMapping("/modelAndViewTest")` 声明处理业务方法，合理使用`Model` ,`ModelView`， `Map` 进行逻辑控制；
> 注意：使用包扫描需要正确使用 `context`命名空间；注意视图解析器的配置，简化路径书写；


### 500异常解决
![](images/image3.png)

> 如上错误，包导入正常，配置的类也正常，可以识别到，但是部署之后无法访问并且跳转界面；重新配置了一下Tomcat以及Artifacts之后解决问题；
初步判定是由于创建了项目运行环境之后，重新改了tomcat或者jar包之后，没有刷新或者修改部署的环境，导致同样的代码出现不同的结果

![](images/image2.png)


### 注解扫描由于头文件缺少元素导致的错误
```
    org.springframework.beans.factory.xml.XmlBeanDefinitionStoreException: Line 8 in XML document from class path resource [springmvc.xml] is invalid; nested exception is org.xml.sax.SAXParseException; lineNumber: 8; columnNumber: 62; cvc-complex-type.2.4.c: 通配符的匹配很全面, 但无法找到元素 'context:component-scan' 的声明。
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.doLoadBeanDefinitions(XmlBeanDefinitionReader.java:399)
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:336)
	org.springframework.beans.factory.xml.XmlBeanDefinitionReader.loadBeanDefinitions(XmlBeanDefinitionReader.java:304)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:181)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:217)
	org.springframework.beans.factory.support.AbstractBeanDefinitionReader.loadBeanDefinitions(AbstractBeanDefinitionReader.java:188)
	org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:125)
	org.springframework.web.context.support.XmlWebApplicationContext.loadBeanDefinitions(XmlWebApplicationContext.java:94)
	org.springframework.context.support.AbstractRefreshableApplicationContext.refreshBeanFactory(AbstractRefreshableApplicationContext.java:129)
	org.springframework.context.support.AbstractApplicationContext.obtainFreshBeanFactory(AbstractApplicationContext.java:609)
	org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:510)
	org.springframework.web.servlet.FrameworkServlet.configureAndRefreshWebApplicationContext(FrameworkServlet.java:668)
	org.springframework.web.servlet.FrameworkServlet.createWebApplicationContext(FrameworkServlet.java:634)
	org.springframework.web.servlet.FrameworkServlet.createWebApplicationContext(FrameworkServlet.java:682)
	org.springframework.web.servlet.FrameworkServlet.initWebApplicationContext(FrameworkServlet.java:553)
	org.springframework.web.servlet.FrameworkServlet.initServletBean(FrameworkServlet.java:494)
	org.springframework.web.servlet.HttpServletBean.init(HttpServletBean.java:136)
	javax.servlet.GenericServlet.init(GenericServlet.java:158)
	org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:491)
	org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)
	org.apache.catalina.valves.AbstractAccessLogValve.invoke(AbstractAccessLogValve.java:668)
```

* 解决方案
![](images/image7.png)

### 注解和XML同时使用 ，注解失效;
* 解决方法 `配置文件中不能存在SimpleUrlHandlerMapping这个Bean的配置，否则注解不起作用`


