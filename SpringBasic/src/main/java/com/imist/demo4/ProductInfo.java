package com.imist.demo4;

/**
 * 用于计算价格，通过表达式使用，同样的也需要用Spring 的Bean管理起来
 */
public class ProductInfo {

    public Double calculatePrice(){
        return Math.random()*199;
    }
}
