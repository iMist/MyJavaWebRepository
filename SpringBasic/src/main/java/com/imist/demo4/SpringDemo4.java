package com.imist.demo4;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDemo4 {

    /**
     * 通过构造器注入属性
     */
    @Test
    public void demo(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = (User) context.getBean("user");

        System.out.println(user.toString());
    }

    /**
     * 通过setter注入属性
     */
    @Test
    public void demo2(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Person person = (Person) context.getBean("person");
        System.out.println(person.toString());
    }

    /**
     * 通过P命名空间注入属性
     */
    @Test
    public void demo3(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Person person = (Person) context.getBean("person");
        System.out.println(person.toString());
    }

    /**
     * 通过SpEL表达式注入属性
     */
    @Test
    public void demo4(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Product product = (Product) context.getBean("product");
        System.out.println(product.toString());
    }
}
