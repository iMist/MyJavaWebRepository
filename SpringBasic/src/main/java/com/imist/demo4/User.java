package com.imist.demo4;

public class User {

    private String name;
    private Integer age;

    //通过构造器方式注入属性的时候一定要加上对应的构造器，不然编译无法通过
    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

}
