package com.imist.demo3;

/*
定义数据库操作接口
 */
public interface UserDao {
    public void findAll();

    public void save();

    public void update();

    public void delete();


}
