package com.imist.demo3;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Bean的作用范围测试
 */
public class SpringDemo3 {
    @Test
    public void demo1(){
        //创建工厂
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例
        Person person = (Person) applicationContext.getBean("person" );
        Person person2 = (Person) applicationContext.getBean("person" );
        System.out.println(person);
        System.out.println(person2);
    }

    /**
     * 多例的Bean
     */
    @Test
    public void demo2(){
        //创建工厂
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例
        Person person = (Person) applicationContext.getBean("person2" );
        Person person2 = (Person) applicationContext.getBean("person2" );
        System.out.println(person);
        System.out.println(person2);
    }

    /**
     * 完整的man的生命周期方法测试
     */
    @Test
    public void demo3(){
        //创建工厂
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例
        Man man = (Man) applicationContext.getBean("man" );
        //System.out.println(man);
        //执行业务方法
        man.run();
        //关闭工厂
        applicationContext.close();
    }

    /**
     * 用来测试PostProcessor对类进行增强
     */
    @Test
    public void demo4(){
        //创建工厂
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例
        UserDao userDao = (UserDao) applicationContext.getBean("userDao" );
        //System.out.println(man);
        //执行业务方法
        userDao.findAll();
        userDao.save();
        userDao.update();
        userDao.delete();
    }

}
