package com.imist.demo_aop;


import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDemo {
    /**
     * 基础数据类型的属性注入
     */
    @Test
    public void demo(){
        ApplicationContext context = new ClassPathXmlApplicationContext("aopContext.xml");
        UserServices userService = (UserServices) context.getBean("userServices");
        userService.eat();
        System.out.println(userService.sayHello("zhangsan "));
    }

    /**
     * 自定义类型的属性注入
     */
    @Test
    public void demo2(){
        ApplicationContext context = new ClassPathXmlApplicationContext("aopContext.xml");
        UserServices userService = (UserServices) context.getBean("userServices");
        userService.save();
    }

    /**
     * Spring生命周期注解
     */
    @Test
    public void demo3(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("aopContext.xml");
        LivescycleBean livescycle = (LivescycleBean) context.getBean("livescycle");
        livescycle.say();
        context.close();
    }

    /**
     * @Scope("protoType") 作用域测试 使用protoType配置多例，默认singleton
     */
    @Test
    public void demo4(){
        ApplicationContext context = new ClassPathXmlApplicationContext("aopContext.xml");
        ScopeBean scopeBean = (ScopeBean) context.getBean("scopeBean");
        ScopeBean scopeBean2 = (ScopeBean) context.getBean("scopeBean");
        System.out.println(scopeBean);
        System.out.println(scopeBean2);
    }
}
