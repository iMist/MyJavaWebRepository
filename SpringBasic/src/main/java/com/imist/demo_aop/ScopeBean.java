package com.imist.demo_aop;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("scopeBean")
@Scope("prototype") //默认singleton
public class ScopeBean {
}
