package com.imist.demo_aop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 相当于<bean id = "userBean" class = ""></bean>
 */
//@Component("userServices")
@Service("userServices")
public class UserServices {

    /**
     * 基本数据类型用@Value注解即可，若是没有set方法就加载属性上，若是有set方法还可以加在set方法上
     */
    @Value("米饭")
    private String something;
    /**
     * @Autowired 默认按照类型注入（和名称无关）,但若是存在两个相同Bean类型相同，则按照名称注入
     * @Qualifier 指定注入名称,搭配@Autowired强制按照名称注入
     * @Resource 和 @Autowired 注解功能类似,强制按照名称注入
     */
    /*@Autowired
    @Qualifier("userDao")*/
    @Resource(name = "userDao")
    private UserDao userDao;

    public String sayHello(String name){
        return "hello " + name;
    }

    public void eat(){
        System.out.println("eat " + something);
    }

    public void save(){
        userDao.save();
        System.out.println("Service 保存用户 ..." );
    }
}
