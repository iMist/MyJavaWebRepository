package com.imist.demo_aop;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component("livescycle")
public class LivescycleBean {

    public LivescycleBean() {
        System.out.println("bean被实例化");
    }

    /**
     * bean被装载到容器中的时候调用
     */
    @PostConstruct
    public void init(){
        System.out.println("initBean");
    }
    public void say(){
        System.out.println("say hello!");
    }

    /**
     * bean从入其中被移除的时候调用
     */
    @PreDestroy
    public void destory(){
        System.out.println("destory Bean");
    }
}
