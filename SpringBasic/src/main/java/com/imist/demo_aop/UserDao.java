package com.imist.demo_aop;

import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDao {
    public void save(){
        System.out.println("DAO 中保存用户");
    }
}
