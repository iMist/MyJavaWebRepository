package com.imist.demo2;

/**
 * Bean3 的实例工厂
 */
public class Bean3Factory {
    /**
     * 实例工厂方法（非静态）
     * @return
     */
    public  Bean3 createBean(){
        System.out.println("Bean3 的实例工厂执行...");
        return new Bean3();
    }
}
