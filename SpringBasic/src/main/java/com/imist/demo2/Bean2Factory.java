package com.imist.demo2;

/**
 * Bean2的静态工厂
 */
public class Bean2Factory {
    public static Bean2 createBean2(){
        System.out.println("BeanFactory 静态工厂方法执行");
        return new Bean2();
    }
}
