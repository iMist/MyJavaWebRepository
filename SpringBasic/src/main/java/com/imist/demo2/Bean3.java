package com.imist.demo2;

/**
 * Bean 实例化的三种方式：实例工厂实例化
 */
public class Bean3 {

    public Bean3() {
        System.out.println("Bean3 被创建了");
    }
}
