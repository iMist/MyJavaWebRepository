package com.imist.demo2;

/**
 * Bean实例化三种方式：静态工厂实例化；
 */
public class Bean2 {

    public Bean2() {
        System.out.println("Bean2 被实例化了");
    }
}
