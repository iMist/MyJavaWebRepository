package com.imist.xml_aop;


import javax.annotation.Resource;

public class ProductService {

    @Resource(name = "categoryDao")
    private CategoryDao categoryDao;
    @Resource(name = "productDao")
    private ProductDao productDao;

    //通过注解注入属性不需要geter和setter方法

    /*public CategoryDao getCategoryDao() {
        return categoryDao;
    }

    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    public ProductDao getProductDao() {
        return productDao;
    }

    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }*/

    public void save(){
        System.out.println("ProductService 商品保存操作 save....");
        categoryDao.find();
        productDao.save();
    }
}
