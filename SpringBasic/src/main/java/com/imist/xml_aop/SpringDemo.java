package com.imist.xml_aop;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringDemo {
    @Test
    public void demo(){
        ApplicationContext context = new ClassPathXmlApplicationContext("aop_xml_Context.xml");
        ProductService productService = (ProductService) context.getBean("productService");
        productService.save();

    }
}
