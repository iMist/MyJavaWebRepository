package com.imist.demo1;


import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

public class SpringTest {
    /**
     * 传统方式
     */
    @Test
    public void demo1(){
        //UserService userService = new UserServiceImpl();
        //传统方式设置属性 ，需要将抽象类修改成为实现类，然后调用set方法，需要修改代码实现
        UserServiceImpl userService = new UserServiceImpl();
        userService.setName("李四");
        userService.sayHello();
    }

    /**
     * Spring的方式实现
     */
    @Test
    public void demo2(){
        // 1.创建Spring的工厂
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例化对象，根据指定的ID获取
        UserService userService = (UserServiceImpl) context.getBean("userService");
        //Spring方式 不需要修改代码即可实现属性注入；
        userService.sayHello();
    }

    /**
     * 读取磁盘系统中的配置文件
     */
    @Test
    public void demo3(){
        // 1.创建Spring的工厂
        ApplicationContext context = new FileSystemXmlApplicationContext("/Users/imist/MyRepository/MyJavaWebRepository/SpringBasic/src/main/resources/applicationContext.xml");
        //通过工厂获得类的实例化对象，根据指定的ID获取
        UserService userService = (UserServiceImpl) context.getBean("userService");
        //Spring方式 不需要修改代码即可实现属性注入；
        userService.sayHello();
    }

    /**
     * 传统方式的工厂类 资源配置（老版本）
     */
    @Test
    public void demo4(){
        BeanFactory factory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        //其他的方式都一样
        UserService userService = (UserServiceImpl) factory.getBean("userService");

        userService.sayHello();
    }
    /**
     * 传统方式的工厂类，文件系统配置（老版本）
     */
    @Test
    public void demo5(){
        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("/Users/imist/MyRepository/MyJavaWebRepository/SpringBasic/src/main/resources/applicationContext.xml"));
        //其他的方式都一样
        UserService userService = (UserServiceImpl) factory.getBean("userService");

        userService.sayHello();
    }
}
