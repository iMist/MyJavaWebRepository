    [toc]

### 相关类容
* Spring的快速配置 [Spring快速配置.md](Spring快速配置.md)
* Spring的Bean管理 [Spring的Bean管理](Spring的Bean管理.md)

### Spring 概述
1. Spring 是一个开源框架；
2. Spring 为简化企业级应用开发，使用Spring可以实现简单的Javabean,实现以前只有EJB 才能实现的功能；
3. Spring是JavaSE /javaEE一站式框架；

### Spring 的优点；
- 方便解耦，简化开发
    * Spring就是一个大工厂，可以将所有的对象创建和依赖关系的维护交给Spring管理
    
- aop编程的支持
    * Spring 提供面向切面的编程，可以方便实现对程序进行权限拦截，运行监控等功能；

- 声明式事务的支持
    * 只需要通过配置就可以完成对事务的支持，无需手动编程；
 
- 方便程序的测试
    * Spring对Junit4 的支持可以通过注解的方式很方便的测试Spring程序；
   
- 方便集成各种优秀的框架；

   * Spring不排斥各种优秀的框架，其内部提供了对各种优秀框架（如：struts.hibernate,Mybatis 等）的直接支持
   
- 降低了JavaEE的使用难度；
    * Spring对JavaEE的开发中非常难用的api（jdbc,javamail,远程调用等都提供了直接封装，使这些API的应用难度大大降低）；
  
  
### Spring 的模块
![](images/image1.png)

### Spring 的ioc底层原理实现

* IOC和DI也就是控制反转是同一回事；
* 对象被动的接受依赖类;
* SpringIOC涉及到了工厂模式和反射模式

![](images/image2.png);


### 控制反转与依赖注入

* 依赖注入的目的是在代码之外管理程序间组件的依赖关系；依赖注入减低了程序组件间的耦合 

![](images/image4.png)

### Spring 的工厂类结构

![](images/image5.png)







                           
 