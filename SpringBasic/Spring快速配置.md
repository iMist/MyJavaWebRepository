### 导入Spring核心包
```
<dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-expression</artifactId>
            <version>4.2.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
            <version>RELEASE</version>
        </dependency>
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.12</version>
        </dependency>
```
    
    
### 创建Spring核心配置文件

![](images/image3.png)   


### Spring 实现
* 配置文件
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
        
       <!--控制反转 ：UserService 的创建全交给Spring，class 为实现类的全路径-->
       <bean id="userService" class="com.imist.demo1.UserServiceImpl">
           <!-- 依赖注入：设置属性-->
           <property name="name" value="张三"></property>
       </bean>
</beans>
```

* 代码实现
```java
    public class SpringTest {
        // 1.创建Spring的工厂
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例化对象，根据指定的ID获取
        UserService userService = (UserServiceImpl) context.getBean("userService");

        userService.sayHello();
        }
```