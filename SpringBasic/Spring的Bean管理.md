
## Spring的Bean管理

##### 一般情况下基本一致，但是name可以包含特殊符号；
![](images/image6.png)

##### Bean的作用域,默认作用域`singleton`
![](images/image7.png)

##### Bean的生命周期

* Spring初始化或者销毁bean的时候，有时候需要做一些处理操作，因此Spring可以再创建或者销毁Bean的时候调用两个声明的生命周期方法

![](images/image8.png)

> 注意：只有当作用域是单例的时候才有效；关闭工厂的时候才调用销毁的方法
```java
        //创建工厂,这里需要用实现类，不然工厂没有close()方法
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过工厂获得类的实例
        Man man = (Man) applicationContext.getBean("man" );
        System.out.println(man);
        applicationContext.close();
```

#### Bean在容器中的生命周期
![](images/image9.png)

![](images/image10.png)

![](images/image11.png)
* 最后一步11.执行配置的工厂销毁之后调用的单例bean销毁的方法

* 注意最重要的是第五步和第八步，可以利用代理对类进行增强；

* 每一个bean被实例化都会经历上述方法


### Spring的属性注入
* 对于类成员变量，注入方式有三种
    * 构造函数注入:Spring配置文件中通过`<constructor-arg>`设置属性
    * 属性setter方法注入:Spring配置文件中通过 `<property>` 设置属性
    * 接口注入
> Spring支持前两种;普通类型用 `value`  ,对象类型用 `ref`

```xml 
    <bean id="person" class="com.imist.demo4.Person">
         <!--普通属性用value-->
        <property name="name" value="lisi"/>
        <property name="age" value="23"/>
         <!--引用其他类型的ID或者name用ref-->
         <property name="cat" ref="cat"/>
     </bean>
     <bean id="cat" class="com.imist.demo4.Cat">
             <property name="name" value="ketty"></property>
         </bean>
  ```
  
#### Spring属性注入通过-p名称空间
![](images/image12.png)
* 引入p名称空间 `xmlns:p="http://www.springframework.org/schema/p"`
* 普通类型属性通过 `p:name`赋值，对象类型通过`XX-ref`的方式赋值  

#### SpEL 表达式进行属性注入
![](images/image13.png)

#### 复杂类型的属性注入
 
* 数组,集合和properties类型的属性注入 简单类型用`value`对象类型用`ref`
![](images/image14.png)


####  Xml 配置方式
[xml配置进行bean管理](src/main/resources/applicationContext.xml);


#### 注解方式
[xml配置进行bean管理](src/main/resources/aopContext.xml)
* 一定要先引入`xmlns:context="http://www.springframework.org/schema/context" `命名空间
* 一定要开启包扫描,不然无法识别对应包下的注解
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context" xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
</beans>
```
* 基本数据类型用@Value注解即可，若是没有set方法就加载属性上，若是有set方法还可以加在set方法上

* 分层注解 `@Repository` `@Service` `@Controller`等效于 `@Component`
![](images/image15.png)

* 属性注入注解：基本数据类型用@Value注解即可，若是没有set方法就加在属性上，若是有set方法则加在set方法上

![](images/image16.png)

#### 属性注解使用
* @Autowired 默认按照类型注入（和名称无关）,但若是存在两个相同Bean类型相同，则按照名称注入
* @Qualifier("") 指定注入名称,搭配@Autowired强制按照名称注入
* @Resource（name = ""） 和 @Autowired 注解功能类似,强制按照名称注入

#### 其他注解
* 生命周期注解
    - @PostConstruct当Bean被装载到Spring容器中的时候调用，对应xml文件的`destory-method 属性`；
    - @PreDEstory 当Bean从Spring容器中删除的时候调用，对应xml文件的`init-method属性`；
![](images/image17.png)

> 注意：使用注解配置的Bean和配置的Bean一样，默认的作用范围都是singleton
`@Scope注解在类上用于指定Bean的作用范围 @Scope("prototype")`


### Xml方式和注解混合使用
![](images/image18.png)
* `<context:annotation-config/> `只开启属性注解，xml配置管理bean，注解注入属性