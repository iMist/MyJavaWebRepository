package com.imist.dao;

import com.imist.entity.Course;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CourseDao {
    //因为SpringIOC容器默认是单例的，所以CourseDao对象只会有一个实例，courses也就只会有一个实例
    private Map<Integer ,Course> courses = new HashMap<Integer, Course>();

    /**
     * 新增课程
     * @param course
     */
    public void addCourse(Course course){
        courses.put(course.getId(),course);
    }

    /**
     * 查询所有课程
     * @return
     */
    public Collection<Course> findAll(){
        return courses.values();
    }

    /**
     * 根据ID获得指定的课程
     * 若是给客户端接口的话最好用Integer,可是用自己的页面展示，可以用基本数据类型，因为客户端无法更改
     * @param id
     * @return
     */
    public Course getCourseById(int id){
        return courses.get(id);
    }

    /**
     * 修改课程
     * @param course
     */
    public void update(Course course){
        courses.put(course.getId(),course);
    }
    /**
     * 删除课程
     * @param id
     */
    public void delete(Integer id){
        courses.remove(id);
    }

}
