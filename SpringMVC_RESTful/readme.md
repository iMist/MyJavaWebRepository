### RESTful风格概述
* RESTful是什么？
    ![](images/image1.png)
    * restful不是一套标准，只是一种开发方式，架构思想
    * url更简洁
    * 有利于不同操作系统之间的资源共享；
* 核心内容
    * 资源与URI;
    * 资源的表述
    * 状态转移
* RESTful具体来讲就是Http协议的四种形式的基本操作
    * `GET` 获取资源
    * `POST` 新建资源
    * `PUT`  修改资源
    * `DELETE` 删除资源     




### `@PostMapping` 无法被识别的问题
* @GetMapping是一个组合注解，是@RequestMapping(method = RequestMethod.GET)的缩写。该注解将HTTP Get映射到特定的处理方法上。

* 同理@PostMapping也是一个组合注解，是@RequestMapping(method=RequestMethod.POST)的缩写。然而在项目中为什么可以使用@RequestMapping注解，不能使用@PostMapping注解呢？

 
通过翻阅资料介绍：@GetMapping，@PostMapping是在Spring 4.3之后新增加的注解，然后回馈到项目中发现，所用的Jar包是Spring 4.2版本。

> 解决办法：采用Spring4.3 之后的jar包

### 将post或者get请求转换成为符合RESTful类型的请求

1. 需要添加Spring内置过滤器
```xml
    <filter>
        <filter-name>hiddenHttpMethodFilter</filter-name>
        <filter-class>org.springframework.web.filter.HiddenHttpMethodFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>hiddenHttpMethodFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
```
2. 通过隐藏域将请求方式进行转换
```jsp
          <form action="${pageContext.request.contextPath}/delete/${course.id}" method="post">
              <button class="btn btn-danger btn-sm delete_btn" type="submit">
                                    <%--通过隐藏域，将post请求转换成delete请求--%>
                                    <input type="hidden" name="_method" value="DELETE"/>
                                    <span class="glyphicon glyphicon-trash">删除</span>
                                </button>
                            </form>
```

#### 注意路径错误导致的405 错误

* 如下代码 导致的访问路径最终为 `http://localhost:8080/delete/getAll`，从而导致405错误；

   ```Java
    @RequestMapping(value = "/delete/{id}" , method = RequestMethod.DELETE )
    public String delete(@PathVariable("id") int id){
        courseDao.delete(id);
        return "redirect:getAll";
    }
    ```
   
 > 因为重定向的路径是从当前路径开始的，解决方案：改代码为 `return "redirect:/getAll";从根路径开始  `