package com.imist.utils;

import java.util.UUID;

public class UploadUtils {
    /**
     *  生成唯一的文件名
     */
    public static String getUUIDFileName(String fileName){
        int index = fileName.indexOf(".");
        String extention = fileName.substring(index);
        String uuidFileName = UUID.randomUUID().toString().replace("-","")+extention;
        return uuidFileName;
    }
}
