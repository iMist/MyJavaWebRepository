package com.imist.utils;

import javax.servlet.http.Cookie;

public class CookieUtils {
    public static Cookie findCookie(Cookie[] cookies ,String cookieName){
        if (cookies == null)
            //说明客户端浏览器没有携带cookie
            return null;
        else {
            //说明客户端带cookie
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)){
                    return cookie;
                }
            }
            //没有我要找的cookeie
            return null;
        }
    }
}
