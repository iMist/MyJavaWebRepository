<%@page import="com.imist.domain.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>登录成功</title>
    <link rel="stylesheet" href="./css/login.css">
</head>
<body>
<%
    if (session.getAttribute("user") != null) {
        User user = (User) session.getAttribute("user");
        System.out.println(user.getPath());
        int index = user.getPath().lastIndexOf("/");
        String filename = user.getPath().substring(index+1);
%>
<div class="login">
    <div class="header">
        <h1>登录成功</h1>
    </div>
    <div class="content">
        <table align="center">
            <tr>
                <td align="center"><img width="260" height="260" src="./upload/<%= filename%>"/></td>
            </tr>
            <tr>
                <td align="center">欢迎<%= user.getNickname()%>,登录成功！</td>
            </tr>
        </table>

    </div>
</div>

<%
    } else {

        %>
        <div class="login">
            <div class="header">
                <h1>当前未登录，请去<a href="/login.jsp">登录</a></h1>
            </div>
        </div>
<%
    }
%>

</body>
</html>