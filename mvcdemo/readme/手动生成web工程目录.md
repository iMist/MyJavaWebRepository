### IDEA手动生成Web工程目录

1. 创建一个Java项目（这个不多说）
    
2. 在src/main/下创建webapp目录 ，然后在webapp目录下创建WEB-INF目录
        这里的webapp要和java ，resource同级
        
3. 打开File ->ProjectStructure (如下图)，没有web的情况下需要点击+创建一个
    
   ![image](images/image1.png)
   
4.设置好之后，点击ok,没有创建的话点击一下编译即可 此时，webapp目录就被识别了


5.若是资源目录和源码目录没有的话需要创建一个 ，然后选择一下对应的目录即可

![image2](images/image2.png)
    
    
    
    