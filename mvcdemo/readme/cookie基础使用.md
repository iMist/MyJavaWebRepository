### cookie 的基础使用

## 添加cookie
```
                            if (remember.equals("true")){
                            // 完成记住用户名的功能,默认cookie关闭浏览器就没有了，需要设置一下有效期
                            Cookie cookie = new Cookie("username",user.getUsername());
                            //设置有效路径和有效时间
                            cookie.setPath("/mvc_demo");
                            cookie.setMaxAge(60*60*24);
                            //将cookie写回到浏览器
                            resp.addCookie(cookie);
                            }                    
```

## 获取使用cookie

```
            //重request对象中获取cookie
            Cookie[] cookies = request.getCookies();
            //找到对应的cookie
            Cookie cookie = CookieUtils.findCookie(cookies,"username");
            if (cookie != null){
                username = cookie.getValue();
            }
```