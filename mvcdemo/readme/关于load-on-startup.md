
```xml
    <servlet>
        <servlet-name>InitServlet</servlet-name>
        <servlet-class>com.imist.servlet.InitServlet</servlet-class>
        <!--服务启动的时候就加载-->
        <load-on-startup>2</load-on-startup>
    </servlet>
```

1. load-on-startup 元素标记容器是否应该在启动的时候加载这个servlet，(实例化并调用其init()方法)。

2. 它的值必须是一个整数，表示servlet应该被载入的顺序

3. 如果该元素不存在或者这个数为负时，则容器会当该Servlet被请求时，再加载。

4. 当值为0或者大于0时，表示容器在应用启动时就加载并初始化这个servlet；

5. 正数的值越小，该servlet的优先级越高，应用启动时就越先加载。

6. 当值相同时，容器就会自己选择顺序来加载。