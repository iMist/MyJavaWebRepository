package com.imist.servlet;

import com.imist.data.LocalCache;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "BrowseServlet",urlPatterns = {"/browse/list.do","/browse/delete.do"})
public class BrowseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       if (Objects.equals(req.getServletPath(),"/browse/list.do")){
           //从cookie中取出全部，根据id在localcache中查询出对应的全部product
           //将product全部放入list中，并且写入request;
           req.setAttribute("browse",LocalCache.getBrowseLogs());
           req.getRequestDispatcher("/WEB-INF/views/biz/browse_list.jsp").forward(req,resp);
       }else if (Objects.equals(req.getServletPath(),"/browse/delete.do")){
           //从request中取出cookie(按照cookie的name匹配)
           //从value字符串中删除对应的id，把剩余的数据重新放入cookie
           String productId = req.getParameter("productId");
           if (null != productId){
               LocalCache.delBrowseLog(Long.valueOf(productId));
           }
           resp.sendRedirect("/browse/list.do");
       }
    }
}
