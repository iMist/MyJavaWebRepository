package com.imist.servlet;

import com.imist.data.LocalCache;
import com.imist.data.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class DetailServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (Objects.equals(req.getServletPath(),"/detail/detail.do")){
            String productId = req.getParameter("productId");
            if (null != productId){
//                Cookie cookie = new Cookie("productIds","/");
//                resp.addCookie(cookie);
                //判断cookie是否存在，若是存在，则取出已有的，并读出原数据
                //若不存在，则新建
                Product product = LocalCache.getProduct(Long.valueOf(productId));
                req.setAttribute("product",product);
                LocalCache.addBrowseLog(product);
            }
        }
        req.getRequestDispatcher("/WEB-INF/views/biz/detail.jsp").forward(req,resp);
    }
}
