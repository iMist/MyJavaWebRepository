package com.imist.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class LocalCache {
    private static Map<Long,Product> productMap = new HashMap<>();

    private static Map<Long,Cart> cartMap = new HashMap<>();

    private static Map<Long,Product> favoriteMap = new HashMap<>();

    private static Map<Long,Product> browseLogMap = new HashMap<>();

    static {
        productMap.put(1l,new Product(1l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(2l,new Product(2l,"HTML/CSS","java基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(3l,new Product(3l,"HTML/CSS","java基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(4l,new Product(4l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(5l,new Product(5l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(6l,new Product(6l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(7l,new Product(7l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(8l,new Product(8l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(9l,new Product(9l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(10l,new Product(10l,"HTML/CSS","java基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(11l,new Product(11l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));

        productMap.put(12l,new Product(12l,"HTML/CSS","android基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(22l,new Product(22l,"HTML/CSS","android基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(32l,new Product(32l,"HTML/CSS","android基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(42l,new Product(42l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(52l,new Product(52l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(62l,new Product(62l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(722l,new Product(722l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(82l,new Product(82l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(92l,new Product(92l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(120l,new Product(120l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));
        productMap.put(121l,new Product(121l,"HTML/CSS","HTML+CSS基础课程","html+css基础教程带领大家步步深入学习标签的用法和意义","初级",219));

    }

    public LocalCache() {
    }

    public static List<Product> getProduct(){
        //创建一个新的内存区域
        return new ArrayList<>(productMap.values());
    }
    public static List<Product> getProduct(int page ,int size,String name){
        List<Product> products = new ArrayList<>();
        if (null != name && !name.equals("")){
            productMap.values().forEach(product -> {
                if (product.getName().contains(name)){
                    products.add(product);
                }
            });
        }else {
            products.addAll(productMap.values());
        }
        int start = (page -1) *size;
        int end = products.size() >= size * page ?page*size :products.size();
        return products.subList(start,end);
    }
    public static int getProductCount(String name){
        List<Product> products = new ArrayList<>();
        if (null != name && !name.equals("")){
            productMap.values().forEach(product -> {
                if (product.getName().contains(name)){
                    products.add(product);
                }
            });
        }else {
            products.addAll(productMap.values());
        }
        return products.size();
    }


    public static Product getProduct(Long id){

        return productMap.get(id);
    }

    public static void addCart(Product product){
        if (!cartMap.containsKey(product.getId())){
            cartMap.put(product.getId(),new Cart(product.getId(),product.getId(),product.getName(),product.getPrice(),1));
        }else {
            incrCart(product.getId());
        }

    }
    public static  void dercCart(Long productId){
        boolean result = cartMap.get(productId).decrCount();
        if (result){
            cartMap.remove(productId);
        }
    }
    public static void delCart(Long productId){
        cartMap.remove(productId).incCount();
    }

    public static void incrCart(Long productId){
        cartMap.get(productId).incCount();
    }



    public static List<Cart> getCarts() {
        return new ArrayList<>(cartMap.values());
    }

    public static Cart getCart(Long id){
        return cartMap.get(id);
    }

    public static void addFavorite(Product product) {
        if (!favoriteMap.containsKey(product.getId())){
            favoriteMap.put(product.getId(),product);
        }
    }
    public static void delFavorite(long id) {
        if (favoriteMap.containsKey(id)){
            favoriteMap.remove(id);
        }
    }
    public static List<Product> getFavorites(){
        return new ArrayList<Product>(favoriteMap.values());
    }

    public static void addBrowseLog(Product product) {
        browseLogMap.put(product.getId(),product);
    }

    public static List<Product> getBrowseLogs(){
        return new ArrayList<>(browseLogMap.values());
    }

    public static void delBrowseLog(long productId) {
         browseLogMap.remove(productId);
    }
}
